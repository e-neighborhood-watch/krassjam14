module Node exposing
  ( Id (..)
  , Node
  , Range
  , unId
  , distance2D
  , distance3D
  , nameGen
  , fromId
  )

import Extra.List as List

import Cash exposing
  ( Cash
  )
import Image exposing
  ( Image
  )
import Load exposing
  ( Loaded
  )

import Random


type Id =
  Id Int


unId : Id -> Int
unId (Id n) = n


type alias Node =
  { x :
    Float
  , y :
    Float
  , z :
    Float
  , r :
    Float
  , debt :
    Cash
  , id :
    Id
  , name :
    String
  , image :
    Image
  }

type alias Range =
  { min :
    Id
  , max :
    Id
  }


distance3D : Node -> Node -> Float
distance3D n1 n2 =
  sqrt
    ( (n1.x - n2.x)^2
    + (n1.y - n2.y)^2
    + (n1.z - n2.z)^2
    )


distance2D : { a | x : Float, y : Float } -> { b | x : Float, y : Float } -> Float
distance2D n1 n2 =
  sqrt
    ( (n1.x - n2.x)^2
    + (n1.y - n2.y)^2
    )


fromId : Id -> { a | loadingRequired : Loaded { b | nodes : List Node } } -> Maybe Node
fromId (Id id) model =
  case model.loadingRequired of
    Load.Loading ->
      Nothing
    Load.Ready loaded ->
      List.index
        id
        loaded.nodes


nameGen : Random.Generator String
nameGen =
  Random.map2
   (++)
   ( Random.uniform
     "ALFA"
     [ "ARES"
     , "HUNTER"
     , "NEMO"
     , "CITADEL"
     , "OMEGA"
     , "ALEF"
     , "BEACON"
     , "EAGLE"
     , "HERMES"
     , "BRAVO"
     , "CHARLIE"
     , "FOXTROT"
     , "DRACO"
     , "ARTEMIS"
     , "LEDA"
     , "CASTOR"
     , "EINSTEIN"
     , "MJOLNIR"
     , "NORTH"
     , "PROXIMA"
     , "ULTRA"
     , "BASTION"
     , "PALADIN"
     , "BIG-DADDY"
     , "GRENDEL"
     , "HRUNTING"
     , "HROTHGAR"
     , "SCÁTHACH"
     , "CERES"
     , "PROSPER"
     , "PROSPECT"
     , "VESTA"
     , "SENTRY"
     , "OUTPOST"
     , "GUNNER"
     , "VIRGINIA"
     , "EXPLORER"
     , "VOYAGER"
     , "ENDEAVOR"
     , "EXO"
     , "SCOUT"
     , "PIONEER"
     , "DRAGON"
     , "ORION"
     , "TITAN"
     , "ODYSSEY"
     , "PHOENIX"
     , "LAZARUS"
     , "IKAROS"
     , "ULYSSES"
     , "PATHFINDER"
     , "HORIZONS"
     , "NEW HORIZONS"
     , "DAWN"
     , "KEPLER"
     , "VIKING"
     , "MARINER"
     , "HELIOS"
     , "LUNA"
     , "ORBITER"
     , "ZULU"
     , "ROVER"
     , "CURIOSITY"
     , "APOLLO"
     , "VANGUARD"
     , "ASTRA"
     , "ASTRO"
     , "LOKI"
     , "THOR"
     , "ODIN"
     , "FRIGG"
     , "FREYJA"
     , "DWARF"
     , "HERCULES"
     , "ACHILLES"
     , "RAVEN"
     , "SENTINEL"
     , "VAULT"
     , "HAWK"
     , "OSPREY"
     , "MARX"
     , "ZOROASTER"
     , "OAK"
     , "NORSE"
     , "DRUID"
     , "CUCHULAINN"
     , "BANSHEE"
     , "SIREN"
     , "HYDRA"
     ]
   )
   ( Random.map2
     (++)
     ( Random.map
       String.fromInt
       ( Random.int
         -1829
         -1
       )
     )
     ( Random.uniform
       ""
       [ "X"
       , ""
       , "B"
       ]
     )
   )
