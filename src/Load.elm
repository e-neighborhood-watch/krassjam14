module Load exposing
  ( Loaded (..)
  )

type Loaded a
  = Loading
  | Ready a
