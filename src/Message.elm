module Message exposing
  ( Message (..)
  , MainMenuAction (..)
  , SelectionMenuAction (..)
  , Direction (..)
  )

import Model exposing
  ( Model
  )
import Event exposing
  ( Event
  )
import Port exposing
  ( Port
  )

type Direction
  = Up
  | Down
  | Left
  | Right


type MainMenuAction
  = Refuel
  | Indebt
  | ViewClients
  | ViewPassengers
  | ViewShop
  | ViewModules


type SelectionMenuAction
  = SelectOption
  | NextOption
  | PrevOption
  | BackToMain


type Message
  = ParametersLoaded
    ( Model.LoadedParameters
    , Port
    )
  | ArrowKeyChanged Bool Direction
  | AnimationTick Float
  | NumberKeyPressed Int
  | Embark
  | RandomEvent (Event Model)
  | MainMenuKeyPressed MainMenuAction
  | SelectionMenuKeyPressed SelectionMenuAction
