module Ship exposing
  ( Ship
  , TravelStatus (..)
  , totalWeight
  , totalFuelCapacity
  , totalPassengerSlots
  , totalThrust
  , speed
  , travelStatus
  , hasTranslation
  , hasAtmospherePreference
  , hasPerk
  , canRepair
  , adjustCapacities
  )

import Cash exposing
  ( Cash (..)
  , unCash
  )
import Module exposing
  ( Module
  , Perk
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Passenger
  )
import Weight exposing
  ( Weight (..)
  )


type alias Ship =
  { fuel :
    Float
  , cash :
    Cash
  , passengers :
    List Passenger
  , modules :
    List Module
  , messageLog :
    List String
  }


type TravelStatus
  = OK
  | NeedShip
  | NeedFuel
  | NeedEngine
  | NeedCash
  | SameNode


totalWeight : { a | modules : List Module } -> Weight
totalWeight { modules } =
  modules
  |> List.foldr ((.weight) >> Weight.plus) (Weight 0)


totalFuelCapacity : { a | modules : List Module } -> Float
totalFuelCapacity { modules } =
  modules
  |> List.map (.fuelCapacity)
  |> List.sum


totalPassengerSlots : { a | modules : List Module } -> Int
totalPassengerSlots { modules } =
  modules
  |> List.map (.passengerSlots)
  |> List.sum


totalThrust : { a | modules : List Module } -> Float
totalThrust { modules } =
  modules
  |> List.map (.thrust)
  |> List.sum


speed : { a | modules : List Module } -> Float
speed a =
  let
    thrust : Float
    thrust = totalThrust a
  in
    if
      thrust <= 0
    then
      0
    else
      a
      |> totalWeight
      |> Weight.unWeight
      |> toFloat
      |> (/) thrust


hasTranslation : { a | modules : List Module } -> Bool
hasTranslation =
  hasPerk Module.Translator


hasPerk : Perk -> { a | modules : List Module } -> Bool
hasPerk perk { modules } =
  List.any
    ((.perks) >> List.member perk)
    modules

canRepair : Int -> {a | modules : List Module } -> Bool
canRepair brokenIndex { modules } =
  let
    modulesWOBroken =
      modules
      |> List.indexedMap Tuple.pair
      |> List.filter
         ( Tuple.first
         >> (/=) brokenIndex
         )
      |> List.map Tuple.second
  in
    hasPerk Module.Repair { modules = modulesWOBroken }


hasAtmospherePreference : Passenger.AtmospherePreference -> { a | modules : List Module } -> Bool
hasAtmospherePreference { oxygenPreference, xenonPreference, uf6Preference } ship =
  ( case oxygenPreference of
    Passenger.Neutral ->
      True
    Passenger.Required ->
      hasPerk ( Module.Atmos Passenger.Oxygen ) ship
    Passenger.Forbidden ->
      ( hasPerk Module.AtmosPartition ship )
      ||
        ( hasPerk (Module.Atmos Passenger.Oxygen ) ship
        |> not
        )
  )
  &&
    ( case xenonPreference of
      Passenger.Neutral ->
        True
      Passenger.Required ->
        hasPerk ( Module.Atmos Passenger.Xenon ) ship
      Passenger.Forbidden ->
        ( hasPerk Module.AtmosPartition ship )
        ||
          ( hasPerk ( Module.Atmos Passenger.Xenon ) ship
          |> not
          )
    )
  &&
    ( case uf6Preference of
      Passenger.Neutral ->
        True
      Passenger.Required ->
        hasPerk ( Module.Atmos Passenger.UF6 ) ship
      Passenger.Forbidden ->
        ( hasPerk Module.AtmosPartition ship)
        ||
          ( hasPerk ( Module.Atmos Passenger.UF6 ) ship
          |> not
          )
    )


travelStatus : Ship -> Node -> Node -> TravelStatus
travelStatus { fuel, cash, modules } origin destination =
  if
    origin.id == destination.id
  then
    SameNode
  else if
    modules == []
  then
    NeedShip
  else if
    speed { modules = modules} <= 0
  then
    NeedEngine
  else if
    Node.distance3D origin destination > fuel
  then
    NeedFuel
  else if
    Cash.unCash destination.debt > Cash.unCash cash
  then
    NeedCash
  else
    OK


adjustCapacities : Ship -> Ship
adjustCapacities ship =
  { ship
  | fuel =
    min ship.fuel (totalFuelCapacity ship)
  , messageLog =
    ( if
        List.length ship.passengers >
        totalPassengerSlots ship
      then
        if
          totalPassengerSlots ship > 0
        then
          [ "You are over capacity. You assign quarters to double duty."
          ]
        else
          [ "You have no cabin space left.  Your passengers are now crammed into the cockpit."
          ]
      else
        [ ]
    )
    ++ ship.messageLog
  }
