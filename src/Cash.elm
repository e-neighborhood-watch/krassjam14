module Cash exposing
  ( Cash (..)
  , unCash
  , add
  , subtract
  , map
  , map2
  )


type Cash =
  Cash Int


unCash : Cash -> Int
unCash (Cash x) =
  x


subtract : Cash -> Cash -> Cash
subtract =
  map2 (-)

add : Cash -> Cash -> Cash
add =
  map2 (+)


map : (Int -> Int) -> (Cash -> Cash)
map f (Cash x) =
  Cash (f x)


map2 : (Int -> Int -> Int) -> (Cash -> Cash -> Cash)
map2 p (Cash x) (Cash y) =
  Cash (p x y)

