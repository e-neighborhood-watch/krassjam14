module Main exposing (main)

import Browser
import Browser.Events as BrowserEvent
import Dict
import Json.Decode
import Random

import Cash exposing
  ( Cash (..)
  )
import Config
import Config.Image
import Load exposing
  ( Loaded (..)
  )
import Image
import Message exposing
  ( Message (..)
  )
import Model exposing
  ( Model (..)
  , ModelRecord
  , ShipPosition (..)
  , Info (..)
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Species
  , randomSpeciesList
  , Passenger
  , atmosphereName
  , Breathability (..)
  )
import Port
import Update
import View


nodesGenerator : Random.Generator (List Node)
nodesGenerator =
  let
    nodeGenerator : List Node -> Random.Generator Node
    nodeGenerator prevNodes =
      Random.float 0.01 0.02
      |> Random.andThen
        ( \ r ->
          Random.map5
            (\ x y z name image ->
              { x =
                x
              , y =
                y
              , z =
                z
              , r =
                r
              , debt =
                Cash 0
              , id =
                Node.Id (List.length prevNodes)
              , name =
                name
              , image =
                image
              }
            )
            ( Random.float
              (-1 + Config.sizes.reticule)
              (1 - Config.sizes.reticule)
            )
            ( Random.float
              (-1 + Config.sizes.reticule)
              (1 - Config.sizes.reticule)
            )
            ( Random.float 0.3 -0.3 )
            ( Node.nameGen )
            ( Config.Image.stationGen )
        )
      |> Random.andThen
        ( \ newNode ->
          if
            List.any
              ( \ prevNode ->
                Node.distance2D prevNode newNode <= 2*Config.sizes.reticule
              )
              prevNodes
          then
            Random.lazy ( \ _ -> nodeGenerator prevNodes )
          else
            Random.constant newNode
        )
    fixedNodesGenerator : List Node -> Int -> Random.Generator (List Node)
    fixedNodesGenerator generatedNodes numLeft =
      if
        numLeft <= 0
      then
        Random.constant generatedNodes
      else
        nodeGenerator generatedNodes
        |> Random.andThen
          ( \ newNode ->
            fixedNodesGenerator
              (newNode :: generatedNodes)
              (numLeft - 1)
          )
  in
    Random.int 8 16
    |> Random.andThen
      ( fixedNodesGenerator []
      >> Random.map List.reverse
      )


init : () -> ( Model, Cmd Message )
init _ =
  ( Model
    { loadingRequired =
      Loading
    , statusLog =
      [ "Statuses"
      ]
    , ship =
      { fuel =
        0.0
      , cash =
        Cash 0
      , passengers =
        []
      , modules =
        [ ]
      , messageLog =
        [ ]
      }
    , cursorPosition =
      { x =
        0.0
      , y =
        0.0
      }
    , shipPosition =
      AtPort
        (Node.Id 0)
    , currentInfo =
      None
    , arrowKeys =
      { up = False
      , down = False
      , left = False
      , right = False
      }
    , currentEvent =
      Nothing
    }
  , Random.map3
      (\ nodes species atmosphere ->
        { nodes =
          nodes
        , nodeRange =
          { min =
            Node.Id 0
          , max =
            nodes
              |> List.length
              |> (\ x -> x - 1)
              |> Node.Id
          }
        , species =
          species
        , atmosphere3 =
          atmosphere
        }
      )
      nodesGenerator
      randomSpeciesList
      atmosphereName
    |> Random.andThen
      ( \ loaded ->
        Port.startingGenerator loaded (Node.Id 0)
        |> Random.map (Tuple.pair loaded)
      )
    |> Random.generate ParametersLoaded
  )


numberKeyDecoder : Json.Decode.Decoder Message
numberKeyDecoder =
  Json.Decode.field "key" Json.Decode.string
  |> Json.Decode.andThen
    ( \ key ->
      case key of
        "0" ->
          Json.Decode.succeed 0
        "1" ->
          Json.Decode.succeed 1
        "2" ->
          Json.Decode.succeed 2
        "3" ->
          Json.Decode.succeed 3
        "4" ->
          Json.Decode.succeed 4
        "5" ->
          Json.Decode.succeed 5
        "6" ->
          Json.Decode.succeed 6
        "7" ->
          Json.Decode.succeed 7
        "8" ->
          Json.Decode.succeed 8
        "9" ->
          Json.Decode.succeed 9
        _ ->
          Json.Decode.fail "ignored key"
    )
  |> Json.Decode.map NumberKeyPressed


subscriptions : Model -> Sub Message
subscriptions (Model model) =
  let
    arrowKeyDecoder : Bool -> Json.Decode.Decoder Message
    arrowKeyDecoder keyDown =
      Json.Decode.field "key" Json.Decode.string
      |> Json.Decode.andThen
        ( \ key ->
          case key of
            "ArrowUp" ->
              Json.Decode.succeed Message.Up
            "ArrowDown" ->
              Json.Decode.succeed Message.Down
            "ArrowLeft" ->
              Json.Decode.succeed Message.Left
            "ArrowRight" ->
              Json.Decode.succeed Message.Right
            _ ->
              Json.Decode.fail "ignored key"
        )
      |> Json.Decode.map (ArrowKeyChanged keyDown)

    starMapDiscreteKeys : Json.Decode.Decoder Message
    starMapDiscreteKeys =
      Json.Decode.field "key" Json.Decode.string
      |> Json.Decode.andThen
        ( \ key ->
          case key of
            "e" ->
              Json.Decode.succeed Embark
            _ ->
              Json.Decode.fail "ignored key"
        )

    mainMenuKeyDecoder : Json.Decode.Decoder Message.MainMenuAction
    mainMenuKeyDecoder =
      Json.Decode.field "key" Json.Decode.string
      |> Json.Decode.andThen
        ( \ key ->
          case key of
            "r" ->
              Json.Decode.succeed Message.Refuel
            "v" ->
              Json.Decode.succeed Message.ViewClients
            "p" ->
              Json.Decode.succeed Message.ViewPassengers
            "s" ->
              Json.Decode.succeed Message.ViewShop
            "l" ->
              Json.Decode.succeed Message.ViewModules
            "d" ->
              Json.Decode.succeed Message.Indebt
            _ ->
              Json.Decode.fail "ignored key"
        )

    selectionMenuKeyDecoder : Json.Decode.Decoder Message.SelectionMenuAction
    selectionMenuKeyDecoder =
      Json.Decode.field "key" Json.Decode.string
      |> Json.Decode.andThen
        ( \ key ->
          case key of
            "c" ->
              Json.Decode.succeed Message.SelectOption
            "," ->
              Json.Decode.succeed Message.PrevOption
            "." ->
              Json.Decode.succeed Message.NextOption
            "m" ->
              Json.Decode.succeed Message.BackToMain
            _ ->
              Json.Decode.fail "ignored key"
        )

    starMapKeys : Sub Message
    starMapKeys =
      Sub.batch
        [ True
        |> arrowKeyDecoder
        |> BrowserEvent.onKeyDown
        , False
        |> arrowKeyDecoder
        |> BrowserEvent.onKeyUp
        , BrowserEvent.onKeyPress starMapDiscreteKeys
        ]

    animating : Bool
    animating =
      model.arrowKeys.up
      || model.arrowKeys.down
      || model.arrowKeys.left
      || model.arrowKeys.right
  in
    Sub.batch
      [ starMapKeys
      , case model.currentInfo of
        EventInfo _ ->
          BrowserEvent.onKeyPress numberKeyDecoder
        StationInfo _ window ->
          case window of
            Port.MainView ->
              mainMenuKeyDecoder
              |> BrowserEvent.onKeyPress
              |> Sub.map MainMenuKeyPressed
            _ ->
              selectionMenuKeyDecoder
              |> BrowserEvent.onKeyPress
              |> Sub.map SelectionMenuKeyPressed
        PassengersInfo _ _ ->
          selectionMenuKeyDecoder
          |> BrowserEvent.onKeyPress
          |> Sub.map SelectionMenuKeyPressed
        ModulesInfo _ _ ->
          selectionMenuKeyDecoder
          |> BrowserEvent.onKeyPress
          |> Sub.map SelectionMenuKeyPressed
        None ->
          Sub.none
      , if
        animating
      then
        BrowserEvent.onAnimationFrameDelta AnimationTick
      else
        Sub.none
      ]


main : Program () Model Message
main =
  Browser.document
    { init = init
    , update =
      Update.update
    , subscriptions = subscriptions
    , view = View.view
    }
