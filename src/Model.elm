module Model exposing
  ( ModelRecord
  , Model (..)
  , ShipPosition (..)
  , Info (..)
  , DirectionSet
  , LoadedParameters
  , unModel
  , idToNode
  , selectedNode
  , infoToMaybe
  )

import Cash exposing
  ( Cash (..)
  )
import Config
import Config.Image
import Event exposing
  ( Event
  )
import Load exposing
  ( Loaded (..)
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Species
  , Passenger
  )
import Port exposing
  ( Port
  )
import Ship exposing
  ( Ship
  )


type ShipPosition
  = AtPort Node.Id
  | InTransit
    { start:
      Node.Id
    , end:
      Node.Id
    , progress:
      Float
    }


type Info
  = EventInfo (Event Model)
  | StationInfo Port Port.StationWindow
  | PassengersInfo Int Info
  | ModulesInfo Int Info
  | None


type alias DirectionSet =
  { up : Bool
  , down : Bool
  , left : Bool
  , right : Bool
  }


type alias LoadedParameters =
  { nodes :
    List Node
  , nodeRange :
    Node.Range
  , species :
    List Species
  , atmosphere3 :
    String
  }


type alias ModelRecord =
  { loadingRequired :
    Loaded LoadedParameters
  , statusLog :
    List String
  , cursorPosition :
    { x :
      Float
    , y :
      Float
    }
  , shipPosition:
    ShipPosition
  , ship :
    Ship
  , currentInfo :
    Info
  , arrowKeys :
    DirectionSet
  , currentEvent :
    Maybe (Event Model)
  }


type Model =
  Model ModelRecord


unModel : Model -> ModelRecord
unModel (Model model) = model


idToNode : ModelRecord -> Node.Id -> Node
idToNode model (Node.Id id) =
  let
    defaultNode =
      { x =
        0
      , y =
        0
      , z =
        0
      , r =
        0
      , debt =
        Cash 0
      , id =
        Node.Id 0
      , name =
        "DEFAULT"
      , image =
        Config.Image.station
      }
  in
    case
      model.loadingRequired
    of
      Loading ->
        defaultNode
      Ready loads ->
        case
          List.head (List.drop id loads.nodes)
        of
          Just node ->
            node
          Nothing ->
            defaultNode -- could also take node 0 here


selectedNode :
  { a
  | loadingRequired :
    Loaded
      { b
      | nodes :
        List Node
      }
  , cursorPosition :
      { x :
        Float
      , y :
        Float
      }
  } -> Maybe Node
selectedNode { loadingRequired, cursorPosition } =
  let
    go : List Node -> Maybe Node
    go nodes =
      case
        nodes
      of
        [ ] ->
          Nothing
        (headNode :: tailNode) ->
          case
            go tailNode
          of
            Nothing ->
              if
                Node.distance2D headNode cursorPosition <
                  Config.sizes.nodeHitBox
              then
                Just headNode
              else
                Nothing
            Just recordNode ->
              if
                Node.distance2D headNode cursorPosition <
                Node.distance2D recordNode cursorPosition
              then
                Just headNode
              else
                Just recordNode
  in
    case
      loadingRequired
    of
      Loading ->
        Nothing
      Ready { nodes } ->
        go nodes


infoToMaybe : (Port -> Port.StationWindow -> a) -> (Event Model -> a) -> (Int -> a) -> (Int -> a) -> Info -> Maybe a
infoToMaybe portFxn eventFxn passengersFxn modulesFxn info =
  case info of
    StationInfo portData window ->
      window
      |> portFxn portData
      |> Just
    EventInfo event ->
      event
      |> eventFxn
      |> Just
    PassengersInfo index _ ->
      index
      |> passengersFxn
      |> Just
    ModulesInfo index _ ->
      index
      |> modulesFxn
      |> Just
    None ->
      Nothing
