module Local exposing
  ( text
  )



type alias Localization =
  { cash :
    String
  , weight :
    String
  , fuel :
    String
  , passengers :
    String
  , destination :
    String
  , needs :
    String
  , translation :
    String
  , strangePlace :
    String
  , speed :
    String
  }


text : Localization
text =
  en


en : Localization
en =
  { cash =
    "CASH"
  , weight =
    "WEIGHT"
  , fuel =
    "FUEL"
  , passengers =
    "PASSENGERS"
  , speed =
    "SPEED"
  , destination =
    "DESTINATION"
  , needs =
    "NEEDS"
  , translation =
    "TRANSLATION"
  , strangePlace =
    "a very mysterious place"
  }


es : Localization
es =
  { en
  | cash =
    "DINERO"
  , weight =
    "PESO"
  , fuel =
    "COMBUSTIBLE"
  , passengers =
    "PASAJEROS"
  , speed =
    "VELOCIDAD"
  , destination =
    "DESTINO"
  , needs =
    "NECESITA"
  , translation =
    "TRADUCCIÓN"
  , strangePlace =
    "un lugar muy extraño"
  }
