module Weight exposing
  ( Weight (..)
  , unWeight
  , plus
  , subtract
  , map
  , map2
  )


type Weight =
  Weight Int


unWeight : Weight -> Int
unWeight (Weight x) =
  x

map : (Int -> Int) -> Weight -> Weight
map f (Weight x) =
  Weight (f x)

map2 : (Int -> Int -> Int) -> Weight -> Weight -> Weight
map2 p (Weight x) (Weight y) =
  Weight (p x y)

plus : Weight -> Weight -> Weight
plus =
  map2 (+)


subtract : Weight -> Weight -> Weight
subtract =
  map2 (-)
