module Passenger exposing
  ( Species
  , randomSpeciesList
  , generator
  , Passenger
  , atmosphereName
  , formatName
  , Breathability (..)
  , Atmosphere (..)
  , AtmospherePreference
  )

import Image exposing
  ( Image
  )

import Random

import Cash exposing
  ( Cash (..)
  )
import Config.Image
import Extra.Random as Random
import Extra.List as List
import Local
import Load exposing
  ( Loaded
  )
import Node exposing
  ( Node
  )

type Atmosphere
  = Oxygen
  | Xenon
  | UF6


type Breathability
  = Required
  | Forbidden
  | Neutral


type alias AtmospherePreference =
  { oxygenPreference :
    Breathability
  , xenonPreference :
    Breathability
  , uf6Preference :
    Breathability
  }


type alias Species =
  { name :
    String
  , atmospherePreference :
    AtmospherePreference
  , needTranslator :
    Bool
  , image :
    Image
  }


type alias Faction =
  { name: String
  , grudges : List String
  , feuds : List String -- ensure factions can't have grudges / feuds against themselves
  }


type BonusCondition
  = SpeedCondition Int
  | LuxuryCondition String
  | DestinationCondition String


type alias Bonus =
  { amount:
    Cash
  , condition:
    BonusCondition
  }

type alias Name =
  { first :
    String
  , last :
    String
  , nick :
    Maybe String
  }

type alias Passenger =
  { species:
    Species
  -- , faction:
  --   Maybe Faction
  -- , luggage:
  --   Luggage
  -- , bonuses:
  --   List Bonus
  , destination :
    Node.Id
  , name :
    Name
  , bid :
    Cash
  }

formatName : Name -> String
formatName { first, nick, last } =
  first
  ++ " "
  ++ ( case
        nick
      of
        Nothing ->
          ""
        Just nickname ->
          "\""
          ++ nickname
          ++ "\" "
     )
  ++ last

randomAtmosphere : Random.Generator AtmospherePreference
randomAtmosphere =
  Random.map3
    AtmospherePreference
    ( Random.weighted
      (13, Required)
      [ (6, Neutral)
      , (1, Forbidden)
      ]
    )
    ( Random.weighted
      (4, Required)
      [ (11, Neutral)
      , (5, Forbidden)
      ]
    )
    ( Random.weighted
      (2, Required)
      [ (5, Neutral)
      , (13, Forbidden)
      ]
    )

atmosphereName : Random.Generator String
atmosphereName =
  Random.uniform
    ( Random.map2
      (++)
      ( Random.uniform
        "Uranium"
        [ "Neptunium"
        , "Caesium"
        , "Potassium"
        , "Aluminum"
        ]
      )
      ( Random.map2
        (++)
        ( Random.uniform
          " Hexa"
          [ " Tetra"
          , " Di"
          , " Tri"
          , " Dimethyl"
          , " Trimethyl"
          , " Chloro"
          , " Methyl"
          , " Fluoro"
          ]
        )
        ( Random.uniform
          "fluoride"
          [ "chloride"
          , "fluorosilicate"
          , "bromide"
          , "sulfate"
          , "nitrate"
          , "acetate"
          , "acrylate"
          ]
        )
      )
    )
    [
    ]
    |> Random.andThen (\ x -> x)


randomName : Random.Generator String
randomName =
  Random.map2
    (++)
    ( Random.map2
      (++)
      ( Random.uniform
        "KL"
        [ "QU"
        , "KY"
        , "B"
        , "IB"
        , "NG"
        , "MB"
        , ""
        , "CH"
        , "TSH"
        , "PS"
        , "HR"
        , "Y"
        , "Z"
        , "GY"
        , "GJ"
        , "C"
        ]
      )
      ( Random.uniform
        "A"
        [ "E"
        , "I"
        , "O"
        , "U"
        , "AU"
        , "OI"
        ]
      )
    )
    ( Random.map2
      (++)
      ( Random.uniform
        "X"
        [ "G"
        , "K"
        , "KK"
        , "SH"
        , "RR"
        , "ʔ"
        , ""
        , "-"
        , "LF"
        ]
      )
      ( Random.uniform
        "ORM"
        [ "AN"
        , "OK"
        , "YYK"
        , "AKI"
        , "ILI"
        , "WM"
        , "ORROK"
        , "UVIAN"
        , "IPECK"
        , "OLD"
        ]
      )
    )


randomSpeciesList : Random.Generator (List Species)
randomSpeciesList =
  randomSpecies
  |> Random.list 5

randomSpecies : Random.Generator Species
randomSpecies =
  Random.map4
    Species
    randomName
    randomAtmosphere
    ( Random.weighted
      (3, False)
      [ (1, True)
      ]
    )
    Config.Image.alienGen

nickName : Random.Generator (Maybe String)
nickName =
  Random.uniform
    ( Nothing |> Random.constant |> Random.constant)
    [ Random.map
      (Random.map Just)
      ( Random.uniform
        (Random.constant "SLICK")
        [ humanFirstName
        , Random.constant "THE BOULDER"
        , Random.constant "JUNIOR"
        , Random.constant "SLY"
        , Random.constant "THE RAT"
        , Random.constant "SNAKE"
        , Random.constant "HOPPER"
        , Random.constant "DEAD EYE"
        , Random.constant "RUSTY EYE"
        , Random.constant "HOTSHOT"
        , Random.constant "THE BRAINS"
        , Random.constant "THE MUSCLES"
        , Random.constant "POINTDEXTER"
        , Random.constant "BALDY"
        , Random.constant "BLACK JACK"
        , Random.constant "CHIPS"
        , Random.constant "LIGHTNING"
        , Random.constant "THE ROCK"
        , Random.constant "THE MOUNTAIN"
        , Random.constant "RED"
        , Random.constant "BLUE"
        , Random.constant "PIPSQUEAK"
        , Random.constant "SHANKS"
        , Random.constant "SKINNY"
        , Random.constant "TINY"
        , Random.constant "BIG SLICK"
        , Random.constant "BOMBER"
        , Random.constant "BOOTS"
        , Random.constant "THE CRAB"
        , Random.map ((++) "BIG ") humanFirstName
        , Random.map ((++) "SLIMEY ") humanFirstName
        , Random.map ((++) "STINKY ") humanFirstName
        ]
      )
    ]
  |> Random.andThen Random.join

humanFirstName : Random.Generator String
humanFirstName =
  Random.uniform
  "TIM"
  [ "JOHN"
  , "MARKUS"
  , "CARL"
  , "JACQUE"
  , "DAVID"
  , "BOB"
  , "AL"
  , "PETE"
  , "BUCK"
  , "SUSAN"
  , "CHRISTINE"
  , "JACKIE"
  ]


humanName : Random.Generator { last : String, first : String }
humanName =
  Random.map2
    ( \ first last ->
      { first =
        first
      , last =
        last
      }
    )
    humanFirstName
    ( Random.uniform
      "BROWN"
      [ "SMITH"
      , "NGUYEN"
      , "PARK"
      , "CARTER"
      ]
    )

alienNamePair : Random.Generator { last : String, first : String }
alienNamePair =
  Random.map2
   ( \ first last ->
     { first =
       first
     , last =
       last
     }
   )
   ( Random.map2
     (++)
     ( Random.uniform
       "KY"
       [ "EK"
       , "IB"
       , "Y"
       , "HU"
       , "Uʔ"
       ]
     )
     ( Random.uniform
       "O"
       [ "I"
       , "Y"
       , "U"
       , "UE"
       ]
     )
   )
   ( Random.map2
     (++)
     ( Random.uniform
       "OO"
       [ "U"
       , "E"
       , "O"
       , "OI"
       ]
     )
     ( Random.uniform
       "QUE"
       [ "XI"
       , "P"
       , "TR"
       , "ʔU"
       , "ʔO"
       ]
     )
   )

alienName : Random.Generator Name
alienName =
  Random.map2
    ( \ { first, last } nick ->
      { first =
        first
      , last =
        last
      , nick =
        nick
      }
    )
    ( Random.uniform
      humanName
      [ alienNamePair
      ]
      |> Random.andThen ( \ x -> x )
    )
    nickName

generateId : Node.Id -> Node.Range -> Random.Generator Node.Id
generateId loc range =
  Random.int (Node.unId range.min) (Node.unId range.max)
  |> Random.map Node.Id
  |> Random.andThen
    ( \ id ->
      if
        id == loc
      then
        Random.lazy (\ _ -> generateId loc range)
      else
        Random.constant id
    )

generator : Node.Id -> { a | nodes : List Node, nodeRange : Node.Range, species : List Species } -> Random.Generator Passenger
generator originId {nodes, species, nodeRange } =
  generateId originId nodeRange
  |> Random.andThen
    ( \ destId ->
      let
        destGenerator : Random.Generator Node.Id
        destGenerator =
          Random.constant destId


        bidGenerator : Random.Generator Cash
        bidGenerator =
          case
            Maybe.map2
              Node.distance3D
              (List.index (Node.unId originId) nodes)
              (List.index (Node.unId destId) nodes)
          of
            Nothing ->
              Random.constant (Cash 0)
            Just distance ->
              Random.float 200 300
              |> Random.map
                 ( (*) distance
                 >> round
                 >> Cash
                 )
      in
        case
          species
        of
          [ ] ->
            Random.map4
              Passenger
              randomSpecies
              destGenerator
              alienName
              bidGenerator
          ( specie1 :: rest) ->
            Random.map4
              Passenger
              ( Random.uniform
                specie1
                rest
              )
              destGenerator
              alienName
              bidGenerator
    )
