module Render.Passenger exposing
  ( render
  )

import Cash exposing
  ( Cash (..)
  )
import Extra.String as String
import Load exposing
  ( Loaded
  )
import Local
import Module exposing
  ( Module
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Passenger
  , Breathability (..)
  , AtmospherePreference
  )
import Ship exposing
  ( hasTranslation
  )


breathabilityList : Breathability -> AtmospherePreference -> Loaded { a | atmosphere3 : String } -> String
breathabilityList b { oxygenPreference, xenonPreference, uf6Preference } loaded =
  ( if
    oxygenPreference == b
  then
    [ "OXYGEN"
    ]
  else
    [ ]
  )
  ++
    ( if
      xenonPreference == b
    then
      [ "XENON"
      ]
    else
      [ ]
    )
  ++
    ( if
      uf6Preference == b
    then
      [ case loaded of
        Load.Loading ->
          "URANIUM HEXAFLUORIDE"
        Load.Ready { atmosphere3 } ->
          String.toUpper atmosphere3
      ]
    else
      [ ]
    )
  |> String.join ", "


render :
  { a
  | loadingRequired :
    Loaded
      { b
      | nodes : List Node
      , atmosphere3 : String
      }
  , ship :
    { c
    | modules : List Module
    }
  } -> Passenger -> List String
render model alien =
  [ Passenger.formatName alien.name
    ++ " ("
    ++ alien.species.name
    ++ ")"
  ]
  ++
    if
      alien.species.needTranslator
      && not (hasTranslation model.ship)
    then
      [ Local.text.needs
        ++ " "
        ++ Local.text.translation
      ]
    else
      (
        [ Local.text.destination
          ++ ": "
          ++
            ( model
            |> Node.fromId alien.destination
            |> Maybe.map (.name)
            |> Maybe.withDefault "a very mysterious place"
            )
          ++ " (BID: "
          ++ String.fromCash alien.bid
          ++ ")"
        ] ++ (
          if
            alien.species.needTranslator
          then
            [ "USING TRANSLATOR"
            ]
          else
            []
        ) ++ (
          if
            alien.species.atmospherePreference.oxygenPreference == Required
            || alien.species.atmospherePreference.xenonPreference == Required
            || alien.species.atmospherePreference.uf6Preference == Required
          then
            [ Local.text.needs
            ++ " "
            ++ breathabilityList Required alien.species.atmospherePreference model.loadingRequired
            ]
          else
            [ ]
          )
        ++
          ( if
            alien.species.atmospherePreference.oxygenPreference == Forbidden
            || alien.species.atmospherePreference.xenonPreference == Forbidden
            || alien.species.atmospherePreference.uf6Preference == Forbidden
          then
            [ "NO "
            ++ breathabilityList Forbidden alien.species.atmospherePreference model.loadingRequired
            ]
          else
            [ ]
          )
      )
