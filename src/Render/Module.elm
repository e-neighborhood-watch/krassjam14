module Render.Module exposing
  ( render
  )

import Cash exposing
  ( Cash (..)
  )
import Config
import Extra.String as String
import Local
import Module exposing
  ( Module
  )



render : a -> Module -> List String
render _ mod =
  [ mod.name
  , "PRICE: "
    ++ String.fromCash mod.price
  , Local.text.weight
    ++ ": "
    ++ String.fromWeight mod.weight
  ] ++ (
    if
      mod.thrust > 0.0
    then
      [ "THRUST: "
        ++ Config.symbols.weight
        ++ Config.symbols.time
        ++ "/"
        ++ Config.symbols.distance
        ++ String.fromFloatRound 3 mod.thrust
      ]
    else
      [ ]
  ) ++ (
    if
      mod.fuelCapacity > 0.0
    then
      [ "FUEL CAPACITY: "
        ++ Config.symbols.distance
        ++ String.fromFloatRound 3 mod.fuelCapacity
      ]
    else
      [ ]
  ) ++ (
    if
      mod.passengerSlots > 0
    then
      [ "PASSENGER SLOTS: "
        ++ String.fromInt mod.passengerSlots
      ]
    else
      [ ]
  )

