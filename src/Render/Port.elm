module Render.Port exposing
  ( render
  )

import Cash exposing
  ( Cash
  )
import Config
import Extra.List as List
import Extra.String as String
import Port exposing
  ( Port
  )
import Load exposing
  ( Loaded
  )
import Model
import Module exposing
  ( Module
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Passenger
  )
import Render.Module as Module
import Render.Passenger as Passenger
import Ship

render :
  { a
  | loadingRequired :
     Loaded
       { b
       | nodes :
         List Node
       , atmosphere3 :
         String
       }
  , cursorPosition :
    { x :
      Float
    , y :
      Float
    }
  , ship :
    { c
    | cash :
      Cash
    , modules :
      List Module
    , passengers :
      List Passenger
    , fuel :
      Float
    }
  } -> Port -> Port.StationWindow -> List String
render model { id, clients, shop, unitFuelPrice, debtTaken } window =
  let
    currentStationName : String
    currentStationName =
      Port.stationName
        id
        model
  in
    case window of
      Port.MainView ->
        [ "The space port of "
        ++ currentStationName
        ] ++ (
          if
            Cash.unCash model.ship.cash > 0
            && Ship.totalFuelCapacity model.ship > model.ship.fuel
          then
            [ "r"
            ++ Config.symbols.keyOptionSeparator
            ++ "Refuel ("
            ++ String.fromCash
               ( min
                 (Cash.unCash model.ship.cash)
                 ( ceiling
                   ( unitFuelPrice
                   * (Ship.totalFuelCapacity model.ship - model.ship.fuel)
                   )
                 )
               |> Cash.Cash
               )
            ++ "; "
            ++ Config.symbols.currency
            ++ String.fromFloat unitFuelPrice
            ++ "/"
            ++ Config.symbols.time
            ++ ")"
            ]
          else
            [ ]
        ) ++
        [ "v"
        ++ Config.symbols.keyOptionSeparator
        ++ "View potential clients..."
        , "p"
        ++ Config.symbols.keyOptionSeparator
        ++ "View current passengers..."
        , "s"
        ++ Config.symbols.keyOptionSeparator
        ++ "Shop parts..."
        , "l"
        ++ Config.symbols.keyOptionSeparator
        ++ "View current ship modules..."
        ] ++ (
          if
            debtTaken
          then
            [ ]
          else
            [ "d"
            ++ Config.symbols.keyOptionSeparator
            ++ "Take on debt (Get "
            ++ String.fromCash (Port.debtCeiling id model)
            ++ " pay back "
            ++ String.fromCash (Config.gameplay.debtRate * (Port.debtCeiling id model |> Cash.unCash |> toFloat) |> ceiling |> Cash.Cash)
            ++ ")"
            ]
        ) ++
        [ "arrow keys"
        ++ Config.symbols.keyOptionSeparator
        ++ "move cursor on star map"
        ]
        ++
        ( case
            Model.selectedNode model
          of
           Just _ ->
             [ "e"
             ++ Config.symbols.keyOptionSeparator
             ++ "Embark"
             ]
           Nothing ->
             [ ]
        )
      Port.ClientView selectedClient ->
        case clients of
          [] ->
            [ "No more clients available at "
            ++ currentStationName
            , "m"
            ++ Config.symbols.keyOptionSeparator
            ++ "Back to station main area"
            ]
          _ ->
            let
              numClients : Int
              numClients = List.length clients

              boundedSelection : Int
              boundedSelection = modBy numClients selectedClient
            in
              [ String.concat
                [ "Potential clients at "
                , currentStationName
                , " ["
                , String.fromInt (boundedSelection + 1)
                , "/"
                , String.fromInt numClients
                , "]"
                ]
                , ""
              ]
                ++
                  ( case List.index boundedSelection clients of
                    Nothing ->
                      [ "Unknown client!"
                      , ""
                      ]
                    Just client ->
                      Passenger.render model client
                      ++ [ "" ]
                      ++
                        ( if
                          List.length model.ship.passengers < Ship.totalPassengerSlots model.ship
                          && Ship.hasAtmospherePreference client.species.atmospherePreference model.ship
                        then
                          [ "c"
                          ++ Config.symbols.keyOptionSeparator
                          ++ "Take on client"
                          ]
                        else
                          []
                        )
                  )
                ++
                  if
                    Config.misc.compactMenus
                  then
                    [ ","
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Previous; ."
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Next; m"
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Back"
                    ]
                  else
                    [ ","
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Previous client"
                    , "."
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Next client"
                    , "m"
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Back to station main area"
                    ]
      Port.PartsView selectedPart->
        case shop of
          [] ->
            [ "No more modules available at "
            ++ currentStationName
            , "m"
            ++ Config.symbols.keyOptionSeparator
            ++ "Back to station main area"
            ]
          _ ->
            let
              numParts : Int
              numParts = List.length shop

              boundedSelection : Int
              boundedSelection = modBy numParts selectedPart
            in
              [ String.concat
                [ "Available modules at "
                , currentStationName
                , " ["
                , String.fromInt (boundedSelection + 1)
                , "/"
                , String.fromInt numParts
                , "]"
                ]
                , ""
              ]
                ++
                  ( case List.index boundedSelection shop of
                    Nothing ->
                      [ "Unknown module!"
                      , ""
                      ]
                    Just part ->
                      Module.render model part
                      ++ [ "" ]
                      ++
                        ( if
                          Cash.unCash part.price <= Cash.unCash model.ship.cash
                        then
                          [ "c"
                          ++ Config.symbols.keyOptionSeparator
                          ++ "Buy module"
                          ]
                        else
                          []
                        )
                  )
                ++
                  if
                    Config.misc.compactMenus
                  then
                    [ ","
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Previous; ."
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Next; m"
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Back"
                    ]
                  else
                    [ ","
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Previous module"
                    , "."
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Next module"
                    , "m"
                    ++ Config.symbols.keyOptionSeparator
                    ++ "Back to station main area"
                    ]

