module Event exposing
  ( PredicateResult (..)
  , Option
  , Event
  , Pooled
  , SpecialPooled
  , generatorFromPool
  , render
  )

import Random

import Config
import Config.Image
import Image exposing
  ( Image
  )


type PredicateResult
  = Success
  | Hidden
  | FailWithReason String


type alias Option a =
  { predicate : a -> PredicateResult
  , effect : a -> a
  , description : String
  }


type alias Event a =
  { description : List String
  , options : List (Option a)
  , image : Image
  }


type alias SpecialPooled a b =
  a -> Maybe (Float, Random.Generator (Event b))


type alias Pooled a =
  SpecialPooled a a


nonEvent : (Float, Random.Generator (Event a))
nonEvent =
  ( 0.0001
  , Random.constant
    { description =
      [ "You experience a moment of perfect stillness..."
      ]
    , options =
      [ { predicate =
          always Success
        , description =
          "Huh, that was strange"
        , effect =
          identity
        }
      ]
    , image =
      Config.Image.emptySpace
    }
  )

generatorFromPool : a -> List (Pooled a) -> Random.Generator (Event a)
generatorFromPool model pool =
  pool
  |> List.filterMap
    ((|>) model)
  |> Random.weighted
    nonEvent
  |> Random.andThen identity


indexToKey : Int -> String
indexToKey n =
  if
    n == 9
  then
    "0"
  else
    if
      n >= 0
      && n < 9
    then
      n + 1
      |> String.fromInt
    else
      ""


render : a -> Event a -> List String
render model { description, options } =
  description
  ++
    ( options
      |> List.indexedMap
        ( \ n option ->
          case
            option.predicate model
          of
            Hidden ->
              Nothing
            FailWithReason reason ->
              indexToKey n
              ++ Config.symbols.keyOptionSeparator
              ++ reason
              |> Just
            Success ->
              indexToKey n
              ++ Config.symbols.keyOptionSeparator
              ++ option.description
              |> Just
        )
      |> List.filterMap identity
    )
