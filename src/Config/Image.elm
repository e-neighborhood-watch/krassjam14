module Config.Image exposing
  ( alienGen
  , emptySpace
  , stationGen
  , station
  , bountyHunterGen
  , asteroid
  )

import Random
import Image exposing
  ( Image
  )

alienGen : Random.Generator Image
alienGen =
  Random.map
    ( Image.build Image.Alien )
    ( Random.int
      0
      22
    )

stationGen : Random.Generator Image
stationGen =
  Random.map
    ( Image.build Image.Station )
    ( Random.int
      0
      1
    )

bountyHunterGen : Random.Generator Image
bountyHunterGen =
  Random.map
    ( Image.build Image.BountyHunter )
    ( Random.int
      0
      1
    )

-- To be used in unused brances of case statements in some places
-- TODO special image just for this case
station : Image
station =
  Image.build Image.Station 0

emptySpace : Image
emptySpace =
  Image.build Image.Event 0

asteroid : Image
asteroid =
  Image.build Image.Event 1
