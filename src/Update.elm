module Update exposing
  ( update
  )

import Cash exposing
  ( Cash (..)
  )
import Config
import Event
import Extra.List as List
import Load
import Message exposing
  ( Message
  )
import Model exposing
  ( Model (..)
  , ModelRecord
  )
import Module exposing
  ( Module
  )
import Node
import Passenger exposing
  ( Passenger
  )
import Port
import Ship exposing
  ( Ship
  , TravelStatus (..)
  )
import Travel exposing
  ( travel
  )


update : Message -> Model -> ( Model, Cmd Message )
update message (Model prevModel) =
  Tuple.mapFirst Model <|
  case message of
    Message.ParametersLoaded ( loaded, startingPort ) ->
      ( { prevModel
        | loadingRequired =
          Load.Ready loaded
        , currentInfo =
          Model.StationInfo startingPort Port.MainView
        }
      , Cmd.none
      )
    Message.ArrowKeyChanged keyDown dir ->
      ( let
          prevDirSet : Model.DirectionSet
          prevDirSet =
            prevModel.arrowKeys
        in
          case dir of
            Message.Up ->
              { prevModel
              | arrowKeys =
                { prevDirSet
                | up =
                  keyDown
                }
              }
            Message.Down ->
              { prevModel
              | arrowKeys =
                { prevDirSet
                | down =
                  keyDown
                }
              }
            Message.Left ->
              { prevModel
              | arrowKeys =
                { prevDirSet
                | left =
                  keyDown
                }
              }
            Message.Right ->
              { prevModel
              | arrowKeys =
                { prevDirSet
                | right =
                  keyDown
                }
              }
      , Cmd.none
      )
    Message.AnimationTick timeDelta ->
      ( let
          boolToFloat : Bool -> Float
          boolToFloat bool =
            case bool of
              True -> 1
              False -> 0

          distanceDelta : { x : Float, y : Float }
          distanceDelta =
            { x =
              Config.speeds.cursor
              * ( boolToFloat prevModel.arrowKeys.right
              - boolToFloat prevModel.arrowKeys.left
              )
            , y =
              Config.speeds.cursor
              * ( boolToFloat prevModel.arrowKeys.down
              - boolToFloat prevModel.arrowKeys.up
              )
            }
        in
          { prevModel
          | cursorPosition =
            { x =
              prevModel.cursorPosition.x + distanceDelta.x
              |> min 1
              |> max -1
            , y =
              prevModel.cursorPosition.y + distanceDelta.y
              |> min 1
              |> max -1
            }
          }
      , Cmd.none
      )
    Message.NumberKeyPressed number ->
      case
        prevModel.currentInfo
      of
        Model.EventInfo { options } ->
          let
            visibleOptions : List (Event.PredicateResult, Model -> Model)
            visibleOptions =
              options
              |> List.map
                ( \ option ->
                  ( option.predicate (Model prevModel)
                  , option.effect
                  )
                )
              |> List.filter
                ( Tuple.first
                >> (/=) Event.Hidden
                )

            optionIndex : Int
            optionIndex =
              if
                number == 0
              then
                9
              else
                number - 1
          in
            case
              List.index optionIndex visibleOptions
            of
              Just (Event.Success, effect) ->
                let
                  chainEvent : ModelRecord -> ModelRecord
                  chainEvent model =
                    case
                      model.currentEvent
                    of
                      Nothing ->
                        model
                      Just event ->
                        case
                          model.currentInfo
                        of
                          Model.None ->
                            { model
                            | currentInfo =
                              Model.EventInfo event
                            }
                          _ ->
                            model
                in
                { prevModel
                | currentInfo =
                  Model.None
                , currentEvent =
                  Nothing
                }
                |> Model
                |> effect
                |> Model.unModel
                |> chainEvent
                |> travel
              _ ->
                (prevModel, Cmd.none)
        _ ->
          (prevModel, Cmd.none)
    Message.Embark ->
      case
        Model.selectedNode prevModel
      of
        Nothing ->
          ( { prevModel
            | ship =
              let
                ship =
                  prevModel.ship
              in
                { ship
                | messageLog =
                  "You must select a node to embark"
                  :: ship.messageLog
                }
            }
          , Cmd.none
          )
        Just destination ->
          case
            prevModel.shipPosition
          of
            Model.InTransit _ ->
              ( prevModel
              , Cmd.none
              )
            Model.AtPort id ->
              case
                prevModel.loadingRequired
              of
                Load.Loading ->
                  ( prevModel
                  , Cmd.none
                  )
                Load.Ready loaded ->
                  case
                    loaded.nodes
                    |> List.drop (Node.unId id)
                    |> List.head
                  of
                    Nothing ->
                      ( prevModel
                      , Cmd.none
                      )
                    Just origin ->
                      case
                        Ship.travelStatus prevModel.ship origin destination
                      of
                        OK ->
                          travel
                            { prevModel
                            | shipPosition =
                              Model.InTransit
                                { start =
                                  id
                                , end =
                                  destination.id
                                , progress =
                                  0
                                }
                            }
                        x ->
                          ( { prevModel
                            | ship =
                              let
                                ship =
                                  prevModel.ship
                              in
                                { ship
                                | messageLog =
                                  ( case
                                      x
                                    of
                                      NeedShip ->
                                        "You need a ship to embark"
                                      NeedFuel ->
                                        "You need more fuel to embark"
                                      NeedEngine ->
                                        "You need an engine to embark"
                                      NeedCash ->
                                        "You need more cash to pay off your debt upon arrival"
                                      SameNode ->
                                        "You are already at that node"
                                      OK ->
                                        "Game is bugged. Please report"

                                  )
                                  :: ship.messageLog
                                }
                            }
                          , Cmd.none
                          )
    Message.RandomEvent event ->
      ( { prevModel
        | currentEvent =
          Just event
        , currentInfo =
          Model.EventInfo event
        }
      , Cmd.none
      )
    Message.MainMenuKeyPressed action ->
      case
        prevModel.currentInfo
      of
        Model.StationInfo station Port.MainView ->
          case action of
            Message.Refuel ->
              (
                let
                  ship =
                    prevModel.ship
                in
                  let
                    fuelCap =
                      Ship.totalFuelCapacity ship
                  in
                    if
                      station.unitFuelPrice * (fuelCap - ship.fuel) < toFloat (Cash.unCash ship.cash)
                    then
                      { prevModel
                      | ship =
                        { ship
                        | fuel =
                          fuelCap
                        , cash =
                          Cash (ceiling (toFloat (Cash.unCash ship.cash) - (station.unitFuelPrice * (fuelCap - ship.fuel))))
                        }
                      }
                    else
                      { prevModel
                      | ship =
                        { ship
                        | fuel =
                          ship.fuel + (toFloat (Cash.unCash ship.cash) / station.unitFuelPrice)
                        , cash =
                          Cash 0
                        }
                      }
              , Cmd.none
              )
            Message.Indebt ->
              ( if
                  station.debtTaken
                then
                  prevModel
                else
                  { prevModel
                  | ship =
                    let
                      ship =
                        prevModel.ship
                    in
                      { ship
                      | cash =
                        Cash.unCash ship.cash
                        + Cash.unCash (Port.debtCeiling station.id prevModel)
                        |> Cash
                      }
                  , currentInfo =
                    Model.StationInfo
                      { station
                      | debtTaken =
                        True
                      }
                      Port.MainView
                  , loadingRequired =
                      case
                        prevModel.loadingRequired
                      of
                        Load.Loading ->
                          Load.Loading
                        Load.Ready loaded ->
                          Load.Ready
                            { loaded
                            | nodes =
                              case
                                List.index (Node.unId station.id) loaded.nodes
                              of
                                Nothing ->
                                  loaded.nodes
                                Just node ->
                                  List.take (Node.unId station.id) loaded.nodes
                                  ++
                                    [ { node
                                      | debt =
                                        ( Config.gameplay.debtRate
                                        * ( Cash.unCash >> toFloat )
                                          ( Port.debtCeiling
                                            station.id
                                            prevModel
                                          )
                                        )
                                        |> ceiling
                                        |> Cash
                                      }
                                    ]
                                  ++ List.drop (Node.unId station.id + 1) loaded.nodes
                            }
                  }
              , Cmd.none
              )
            Message.ViewClients ->
              ( { prevModel
                | currentInfo =
                  0
                  |> Port.ClientView
                  |> Model.StationInfo station 
                }
              , Cmd.none
              )
            Message.ViewPassengers ->
              ( { prevModel
                | currentInfo =
                Model.PassengersInfo 0 prevModel.currentInfo
                }
              , Cmd.none
              )
            Message.ViewShop ->
              ( { prevModel
                | currentInfo =
                  0
                  |> Port.PartsView
                  |> Model.StationInfo station 
                }
              , Cmd.none
              )
            Message.ViewModules ->
              ( { prevModel
                | currentInfo =
                Model.ModulesInfo 0 prevModel.currentInfo
                }
              , Cmd.none
              )
        _ ->
          ( prevModel
          , Cmd.none
          )
    Message.SelectionMenuKeyPressed action ->
      case prevModel.currentInfo of
        Model.StationInfo station (Port.ClientView ix) ->
          case action of
            Message.SelectOption ->
              if
                List.isEmpty station.clients
              then
                ( prevModel
                , Cmd.none
                )
              else
                let
                  passengerIndex : Int
                  passengerIndex =
                    modBy (List.length station.clients) ix

                  before : List Passenger
                  before =
                    List.take passengerIndex station.clients

                  ship : Ship
                  ship =
                    prevModel.ship
                in
                  case List.drop passengerIndex station.clients of
                    [] ->
                      ( prevModel
                      , Cmd.none
                      )
                    ( client :: after) ->
                      if
                        List.length ship.passengers >= Ship.totalPassengerSlots ship
                        ||
                          ( Ship.hasAtmospherePreference client.species.atmospherePreference ship
                          |> not
                          )
                      then
                        ( prevModel
                        , Cmd.none
                        )
                      else
                        ( { prevModel
                          | ship =
                            { ship
                            | passengers =
                              client :: ship.passengers
                            , messageLog =
                              ( if
                                  client.species.needTranslator
                                  && not (Ship.hasTranslation ship)
                                then
                                  Passenger.formatName client.name
                                  ++ " has climbed on board"
                                else
                                  Passenger.formatName client.name
                                  ++ " has climbed on board, headed for "
                                  ++ Port.stationName
                                    client.destination
                                    prevModel
                              )
                              :: ship.messageLog
                            }
                          , currentInfo =
                            Model.StationInfo
                              { station
                              | clients =
                                before ++ after
                              }
                              (Port.ClientView ix)
                          }
                        , Cmd.none
                        )
            Message.PrevOption ->
              ( { prevModel
                | currentInfo =
                  (ix - 1)
                  |> Port.ClientView
                  |> Model.StationInfo station
                }
              , Cmd.none
              )
            Message.NextOption ->
              ( { prevModel
                | currentInfo =
                  (ix + 1)
                  |> Port.ClientView
                  |> Model.StationInfo station
                }
              , Cmd.none
              )
            Message.BackToMain ->
              ( { prevModel
                | currentInfo =
                  Port.MainView
                  |> Model.StationInfo station
                }
              , Cmd.none
              )
        Model.StationInfo station (Port.PartsView ix) ->
          case action of
            Message.SelectOption ->
              if
                List.isEmpty station.shop
              then
                ( prevModel
                , Cmd.none
                )
              else
                let
                  moduleIndex : Int
                  moduleIndex =
                    modBy (List.length station.shop) ix

                  before : List Module
                  before =
                    List.take moduleIndex station.shop

                  ship : Ship
                  ship =
                    prevModel.ship
                in
                  case List.drop moduleIndex station.shop of
                    [] ->
                      ( prevModel
                      , Cmd.none
                      )
                    ( part :: after ) ->
                      if
                        Cash.unCash ship.cash < Cash.unCash part.price
                      then
                        ( prevModel
                        , Cmd.none
                        )
                      else
                        ( { prevModel
                          | ship =
                            { ship
                            | modules =
                              part :: ship.modules
                            , cash =
                              Cash.subtract ship.cash part.price
                            , messageLog =
                              [ "Installed "
                              ++ part.name
                              ++ " on ship"
                              ]
                              ++ ship.messageLog
                            }
                          , currentInfo =
                            Model.StationInfo
                              { station
                              | shop =
                                before ++ after
                              }
                              (Port.PartsView ix)
                          }
                        , Cmd.none
                        )
            Message.PrevOption ->
              ( { prevModel
                | currentInfo =
                  (ix - 1)
                  |> Port.PartsView
                  |> Model.StationInfo station
                }
              , Cmd.none
              )
            Message.NextOption ->
              ( { prevModel
                | currentInfo =
                  (ix + 1)
                  |> Port.PartsView
                  |> Model.StationInfo station
                }
              , Cmd.none
              )
            Message.BackToMain ->
              ( { prevModel
                | currentInfo =
                  Port.MainView
                  |> Model.StationInfo station 
                }
              , Cmd.none
              )
        Model.PassengersInfo index prevInfo ->
          case action of
            Message.PrevOption ->
              ( { prevModel
                | currentInfo =
                  Model.PassengersInfo (index - 1) prevInfo
                }
              , Cmd.none
              )
            Message.NextOption ->
              ( { prevModel
                | currentInfo =
                  Model.PassengersInfo (index + 1) prevInfo
                }
              , Cmd.none
              )
            Message.BackToMain ->
              ( { prevModel
                | currentInfo =
                  prevInfo
                }
              , Cmd.none
              )
            Message.SelectOption ->
              ( prevModel
              , Cmd.none
              )
        Model.ModulesInfo index prevInfo ->
          case action of
            Message.PrevOption ->
              ( { prevModel
                | currentInfo =
                  Model.ModulesInfo (index - 1) prevInfo
                }
              , Cmd.none
              )
            Message.NextOption ->
              ( { prevModel
                | currentInfo =
                  Model.ModulesInfo (index + 1) prevInfo
                }
              , Cmd.none
              )
            Message.BackToMain ->
              ( { prevModel
                | currentInfo =
                  prevInfo
                }
              , Cmd.none
              )
            Message.SelectOption ->
              ( prevModel
              , Cmd.none
              )
        _ ->
          ( prevModel
          , Cmd.none
          )
