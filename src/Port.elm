module Port exposing
  ( Port
  , StationWindow (..)
  , currentImage
  , stationName
  , debtCeiling
  , generator
  , startingGenerator
  )

import Cash exposing
  ( Cash (..)
  )
import Config.Image
import Extra.List as List
import Image exposing
  ( Image
  )
import Load exposing
  ( Loaded
  )
import Local
import Module exposing
  ( Module
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Passenger
  )
import Random

type alias NotModel a b =
  { a
  | loadingRequired :
    Loaded
      { b
      | nodes :
        List Node
      }
  }

type alias Port =
  { id :
    Node.Id
  , image :
    Image
  , clients :
    List Passenger
  , shop :
    List Module
  , unitFuelPrice :
    Float
  , debtTaken :
    Bool
  }


type StationWindow
  = MainView
  | ClientView Int
  | PartsView Int


debtCeiling : Node.Id -> NotModel a b -> Cash
debtCeiling nodeId =
  Node.fromId nodeId
  >> Maybe.map
    ( \ { r } ->
      r * 32768.0
      |> ceiling
      |> Cash
    )
  >> Maybe.withDefault (Cash 0)


stationName : Node.Id -> NotModel a b -> String
stationName nodeId =
  Node.fromId nodeId
  >> Maybe.map (.name)
  >> Maybe.withDefault Local.text.strangePlace


startingGenerator : { a | nodes : List Node, nodeRange : Node.Range, species : List Passenger.Species } -> Node.Id -> Random.Generator Port
startingGenerator loaded id =
  Random.map4
    ( Port id
      ( case
          List.index (Node.unId id) loaded.nodes
        of
          Nothing ->
            Config.Image.station
          Just { image } ->
            image
      )
    )
    ( Passenger.generator id loaded
    |> Random.list 6
    )
    Module.startingPortGenerator
    ( Random.int 10000 20000
    |> Random.map
      ( \ n ->
        toFloat n / 100
      )
    )
    ( Random.constant False
    )



generator : { a | nodes : List Node, nodeRange : Node.Range, species : List Passenger.Species } -> Node.Id -> Random.Generator Port
generator loaded id =
  Random.map4
    ( Port id
      ( case
          List.index (Node.unId id) loaded.nodes
        of
          Nothing ->
            Config.Image.station
          Just { image } ->
            image
      )
    )
    ( Passenger.generator id loaded
    |> Random.list 6 -- TODO make this based on the station size
    )
    ( Module.generator
    |> Random.list 6 -- TODO make this based on the station size
    )
    ( Random.int 10000 20000
    |> Random.map
      ( \ n ->
        toFloat n / 100
      )
    )
    ( Random.constant False
    )


currentImage : Port -> StationWindow -> Image
currentImage { clients, shop, image } window =
  case window of
    MainView ->
      image
    ClientView index ->
      case
        clients
      of
        [ ] ->
          Image.Empty
        _ ->
          case
            List.index (modBy (List.length clients) index) clients
          of
            Nothing ->
              Image.Empty
            Just { species } ->
              species.image
    PartsView index ->
      case
        shop
      of
        [ ] ->
          Image.Empty
        _ ->
          case
            List.index (modBy (List.length shop) index) shop
          of
            Nothing ->
              Image.Empty
            Just x ->
              x.image
