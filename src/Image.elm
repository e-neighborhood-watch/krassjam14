module Image exposing
  ( Type (..)
  , Image (..)
  , build
  , toSrc
  )

import Config

type Type
  = Event
  | Alien
  | Station
  | Module
  | Atmosphere
  | Cabin
  | Engine
  | Music
  | Sheild
  | Tank
  | BountyHunter

type Image
  = Empty
  | File Type String

build : Type -> Int -> Image
build imageType n =
  n
  |> String.fromInt
  |> ( \ x -> x ++ ".png" )
  |> File imageType

toSrc : Image -> String
toSrc image =
  case image of
    Empty ->
      "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
    File imageType file ->
      ( if
        Config.debug.elmReactor
      then
        "/"
      else
        ""
      )
      ++ "assets/img/"
      ++ (
        case
          imageType
        of
          Event ->
            "event"
          BountyHunter ->
            "event/bountyhunter"
          Alien ->
            "alien"
          Station ->
            "station"
          Module ->
            "module/module"
          Engine ->
            "module/engine"
          Sheild ->
            "module/sheild"
          Tank ->
            "module/tank"
          Music ->
            "module/music"
          Cabin ->
            "module/cabin"
          Atmosphere ->
            "module/atmosphere"
      )
      ++ "/"
      ++ file
