module Extra.List exposing
  ( liftA2
  , index
  , wrappedIndex
  )

index : Int -> List a -> Maybe a
index n =
  List.drop n
  >> List.head

wrappedIndex : Int -> a -> List a -> a
wrappedIndex ix head tail =
  let
    go : a -> List a -> Int -> a -> List a -> a
    go chead ctail gix ghead gtail =
      if
        gix <= 0
      then
        ghead
      else
        case gtail of
          [] ->
            go chead ctail (gix - 1) chead ctail
          ( nhead :: ntail ) ->
            go chead ctail (gix - 1) nhead ntail
  in
    case
      List.reverse (head :: tail)
    of
      [] ->
        head
      ( last :: init ) ->
        if
          ix < 0
        then
          go
            last
            init
            ( -1 - ix
            |> modBy (1 + List.length tail)
            )
            last
            init
        else
          go
            head
            tail
            ( modBy (1 + List.length tail) ix
            )
            head
            tail
      

liftA2 : (a -> b -> c) -> List a -> List b -> List c
liftA2 p l1 l2 =
  case l1 of
    [] ->
      []
    (x :: xs) ->
      List.map (p x) l2 ++ liftA2 p xs l2
