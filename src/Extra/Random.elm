module Extra.Random exposing
  ( join
  , traverse
  )

import Random

join : Random.Generator (Random.Generator a) -> Random.Generator a
join =
  Random.andThen ( \ x -> x)

traverse : (a -> Random.Generator b) -> List a -> Random.Generator (List b)
traverse f =
  let
    go : List a -> Random.Generator (List b)
    go l =
      case
        l
      of
        [ ] ->
          Random.constant [ ]
        (x :: xs) ->
          Random.map2
            (::)
            (f x)
            (go xs)
  in
    go
