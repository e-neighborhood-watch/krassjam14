module Extra.String exposing
  ( fromCash
  , fromWeight
  , fromFloatRound
  )

import Config
import Cash exposing
  ( Cash (..)
  )
import Weight exposing
  ( Weight (..)
  )

fromCash : Cash -> String
fromCash (Cash n) =
  Config.symbols.currency
  ++ String.fromInt n


fromWeight : Weight -> String
fromWeight (Weight n) =
  Config.symbols.weight
  ++ String.fromInt n


fromFloatRound : Int -> Float -> String
fromFloatRound decimalPlaces =
  let
    multiplier : Float
    multiplier = 10 ^ toFloat decimalPlaces
  in
    (*) multiplier
    >> round
    >> toFloat
    >> ( \ f -> f / multiplier )
    >> String.fromFloat
