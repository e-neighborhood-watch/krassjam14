module Event.Pool.Journey exposing
  ( pool
  )

import Random

import Cash
import Config.Image
import Event
import Event.Journey
import Event.Journey.Asteroid
import Event.Journey.WanderingTrader.Buy
import Event.Journey.WanderingTrader.Trade
import Event.Journey.Breakdown
import Event.Journey.BountyHunter
import Event.Pool
import Extra.String as String
import Model exposing
  ( Model (..)
  )
import Module
import Passenger
import Ship

pool : List (Event.SpecialPooled (Event.Journey.InProgress (Event.Pool.Course Event.Pool.ModelWrapper)) Model)
pool =
  [ Event.Journey.BountyHunter.event
  , Event.Journey.Breakdown.event
  , Event.Journey.WanderingTrader.Trade.event
  , Event.Journey.WanderingTrader.Buy.event
  , Event.Journey.Asteroid.event
  ]
