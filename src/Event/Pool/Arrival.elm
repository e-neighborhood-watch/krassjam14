module Event.Pool.Arrival exposing
  ( pool
  )

import Random

import Cash exposing
  ( Cash (..)
  )
import Config.Image
import Event
import Event.Pool exposing
  ( ModelWrapper
  , Course
  )
import Extra.List as List
import Extra.String as String
import Load
import Model exposing
  ( Model (..)
  )
import Node
import Passenger
import Port
import Ship exposing
  ( Ship
  )


clearDebt :
  Node.Id
  ->
    { b
    | nodes :
      List
        { c
        | debt :
          Cash
        }
    }
  ->
    ( { b
      | nodes :
        List
          { c
          | debt :
            Cash
          }
      }
    , Cash
    )
clearDebt (Node.Id nodeId) loaded =
  let
    before : List { c | debt : Cash }
    before =
      List.take nodeId loaded.nodes

    rest : List { c | debt : Cash }
    rest =
      loaded.nodes
      |> List.drop nodeId
  in
    case
      rest
    of
      [] ->
        ( loaded
        , Cash 0
        )
      ( node :: after ) ->
        ( { loaded
          | nodes =
            before
            ++
              ( { node
                | debt =
                  Cash 0
                }
              :: after
              )
          }
        , node.debt
        )


dockShip : Node.Id -> Cash -> Ship -> Ship
dockShip end debtPayed ship =
  let
    departingPassengers =
      ship.passengers
      |> List.filter
        ( \ passenger ->
          passenger.destination == end
        )


    remainingPassengers =
      ship.passengers
      |> List.filter
        ( \ passenger ->
          passenger.destination /= end
        )

    income =
      departingPassengers
      |> List.map (.bid)
      |> List.foldl (Cash.add) (Cash 0)
  in
    { ship
    | cash =
      Cash.subtract ship.cash debtPayed
      |> Cash.add income
    , passengers =
      remainingPassengers
    , messageLog =
      List.concatMap
        ( \ passenger ->
          [ Passenger.formatName passenger.name
          ++ " has arrived at their destination."
          , "They pay you "
          ++ String.fromCash passenger.bid
          ]
        )
        departingPassengers
      ++ ship.messageLog
    }



pool : List (Event.SpecialPooled (Course ModelWrapper) Model)
pool =
  [ \ { end, model, loaded } ->
    Just
      ( 1
      , Random.map
        ( \ portData ->
          { description =
            [ "You glide smoothly into port"
            ]
          , options =
            [ { predicate =
                always Event.Success
              , description =
                "We've arrived at "
                ++
                  ( case
                      List.index (Node.unId end) loaded.nodes
                    of
                      Nothing ->
                        "a very mysterious port"
                      Just node ->
                        node.name
                  )
                ++ "!"
              , effect =
                ( \ (Model innerModel) ->
                  let
                    (newLoaded, payed) =
                       clearDebt end loaded
                  in
                    Model
                      { innerModel
                      | shipPosition =
                        Model.AtPort end
                      , ship =
                        dockShip end payed innerModel.ship
                      , currentInfo =
                        Model.StationInfo
                          portData
                          Port.MainView
                      , loadingRequired =
                        Load.Ready
                          newLoaded
                      }
                )
              }
            ]
          , image =
            case
              List.index (Node.unId end) loaded.nodes
            of
              Nothing ->
                Config.Image.station
              Just node ->
                node.image
          }
        )
        ( Port.generator loaded end
        )
      )
  ]
