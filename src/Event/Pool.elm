module Event.Pool exposing
  ( Course
  , ModelWrapper
  )

import Node
import Model


type alias Course a =
  { a
  | start :
    Node.Id
  , end :
    Node.Id
  }


type alias ModelWrapper =
  { model :
    Model.ModelRecord
  , loaded :
    Model.LoadedParameters
  }
