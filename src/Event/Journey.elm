module Event.Journey exposing
  ( InProgress
  )

type alias InProgress a =
  { a
  | progress :
    Float
  }
