module Event.Journey.BountyHunter exposing
  ( event
  )

import Random

import Cash
import Config.Image
import Event exposing
  ( Event
  )
import Event.Journey
import Event.Pool
import Extra.String as String
import Image exposing
  ( Image
  )
import Model exposing
  ( Model (..)
  )
import Passenger exposing
  ( Passenger
  )
import Render.Passenger as Passenger
import Ship

buildStructure : Passenger -> Bool -> Image -> Event Model
buildStructure passenger canOutrun image =
  { description =
    [ "A bounty hunter is trailing you. They announce that they are looking for "
    ++ Passenger.formatName passenger.name
    ]
  , options =
    [ { predicate =
        always Event.Success
      , description =
        "View passenger"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | currentEvent =
              Just
                { description =
                  Passenger.render model1 passenger
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Go back"
                    , effect =
                      ( \ (Model model2) ->
                        Model
                          { model2
                          | currentEvent =
                            buildStructure passenger canOutrun image
                            |> Just
                          }
                      )
                    }
                  ]
                , image =
                  passenger.species.image
                }
            }
        )
      }
    , { predicate =
        always Event.Success
      , description =
        "Give them the passenger"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | ship =
              let
                ship =
                  model1.ship
              in
                { ship
                | messageLog =
                  [ Passenger.formatName passenger.name
                  ++ " was abducted"
                  ]
                  ++ ship.messageLog
                , passengers =
                  List.filter (\p -> p /= passenger) ship.passengers
                }
            , currentEvent =
              Just
                { description =
                  [ "The bounty hunter beams onto your ship, grabs the passenger, and leaves." ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  image
                }
            }
        )
      }
     , { predicate =
        always Event.Success
      , description =
        "Try to outrun them"
      , effect =
        ( \ (Model model1) ->
          if
            canOutrun
          then
            Model
              { model1
              | currentEvent =
                Just
                  { description =
                    [ "They're fast, but you're faster. You manage to narrowly pull away."
                    ]
                  , options =
                    [ { predicate =
                        always Event.Success
                      , description =
                        "Continue on"
                      , effect =
                        identity
                      }
                    ]
                  , image =
                    Config.Image.emptySpace
                  }
              }
          else
            let
              ship =
                model1.ship

              stolen =
                Cash.map2
                  min
                  ( Cash.map ( \ x -> x // 2 ) passenger.bid )
                  ship.cash
            in
              Model
                { model1
                | ship =
                  { ship
                  | messageLog =
                    [ Passenger.formatName passenger.name
                    ++ " was abducted"
                    , String.fromCash stolen
                    ++ " was stolen"
                    ]
                    ++ ship.messageLog
                  , passengers =
                    List.filter (\p -> p /= passenger) ship.passengers
                  , cash =
                    Cash.subtract ship.cash stolen
                  }
                , currentEvent =
                  Just
                    { description =
                      [ "They catch up with you and board your ship by force. They take "
                      ++ Passenger.formatName passenger.name
                      ++ ", and loot "
                      ++ String.fromCash stolen
                      ++ " from you while they're there." ]
                    , options =
                      [ { predicate =
                          always Event.Success
                        , description =
                          "Continue on"
                        , effect =
                          identity
                        }
                      ]
                    , image =
                      image
                    }
                }
        )
      }
    ]
  , image =
    image
  }

event : Event.SpecialPooled (Event.Journey.InProgress (Event.Pool.Course Event.Pool.ModelWrapper)) Model
event { model } =
  Just
    ( 1 + toFloat (List.length model.ship.passengers)
    , case
        model.ship.passengers
      of
        firstPassenger :: passengers ->
          Random.map3
            buildStructure
            ( Random.uniform firstPassenger passengers )
            ( Random.weighted
              ( Ship.speed model.ship, True )
              [ (1, False)
              ]
            )
            Config.Image.bountyHunterGen
        [] ->
          Random.constant
            { description =
              [ "A ship seems to be following you for a moment, but then they leave without incident."
              ]
            , options =
              [ { predicate =
                  always Event.Success
                , description =
                  "Continue onwards"
                , effect =
                  identity
                }
              ]
            , image =
              Config.Image.emptySpace
            }
        )
