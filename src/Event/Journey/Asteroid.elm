module Event.Journey.Asteroid exposing
  ( event
  )

import Random

import Cash
import Config.Image
import Event
import Extra.String as String
import Model exposing
  ( Model (..)
  )

event model =
  Just
    ( 0.5
    , Random.map
      ( \ cashAmount ->
        { description =
          [ "You spot an asteroid made of potentially valuable minerals!"
          ]
        , options =
          [ { predicate =
              always Event.Success
            , description =
              "Collect it"
            , effect =
              ( \ (Model model1) ->
                Model
                  { model1
                  | ship =
                    let
                      ship =
                        model1.ship
                    in
                      { ship
                      | cash =
                        Cash.add ship.cash cashAmount
                      }
                  , currentEvent =
                    Just
                      { description =
                        [ "You find "
                        ++ String.fromCash cashAmount
                        ++ " worth of materials."
                        ]
                      , options =
                        [ { predicate =
                            always Event.Success
                          , description =
                            "Continue on"
                          , effect =
                            identity
                          }
                        ]
                      , image =
                        Config.Image.asteroid
                      }
                  }
              )
            }
          ]
          , image =
            Config.Image.asteroid
          }
        )
        (Random.int 10 100 |> Random.map Cash.Cash)
      )
