module Event.Journey.Breakdown exposing
  ( event
  )

import Random

import Config.Image
import Event
import Event.Journey
import Event.Pool
import Image
import Model exposing
  ( Model (..)
  )
import Module exposing
  ( Module
  )
import Ship
import Render.Module as Module

buildStructure : (Int, Module) -> Float -> (Int, Module) -> Event.Event Model
buildStructure
  (brokenModuleIndex, brokenModule)
  repairResult
  (sacrificialModuleIndex, sacrificialModule)
  =
  { description =
    [ "Your "
    ++ brokenModule.name
    ++ " is malfunctioning."
    ]
  , options =
    [ { predicate =
        always Event.Success
      , description =
        "View part"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | currentEvent =
              Just
                { description =
                  Module.render () brokenModule
                , options =
                  [ { predicate =
                      always Event.Success
                    , description = "Go back"
                    , effect =
                      ( \ (Model model2) ->
                         Model
                           { model2
                           | currentEvent =
                             buildStructure
                               ( brokenModuleIndex
                               , brokenModule
                               )
                               repairResult
                               ( sacrificialModuleIndex
                               , sacrificialModule
                               )
                             |> Just
                           }
                      )
                    }
                  ]
                , image =
                  brokenModule.image
                }
            }
        )
      }
    , { predicate =
        always Event.Success
      , description =
        "Jettison it"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | ship =
              let
                ship =
                  model1.ship
              in
                { ship
                | messageLog =
                  [ brokenModule.name
                  ++ " was jettisoned"
                  ]
                  ++ ship.messageLog
                , modules =
                  List.filter (\p -> p /= brokenModule) ship.modules
                }
                |> Ship.adjustCapacities
            , currentEvent =
              Just
                { description =
                  [ "You throw the broken part into the depths of space."
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
        )
      }
    , { predicate =
        always Event.Success
      , description =
        "Try to fix it"
      , effect =
        ( \ (Model model1) ->
           if
             repairResult < 0.333
           then
             Model
               { model1
               | currentEvent =
                 Just
                   { description =
                     [ "You manage to patch the part. It will hold, for now."
                     ]
                   , options =
                     [ { predicate =
                         always Event.Success
                       , description =
                         "Continue on"
                       , effect =
                         identity
                       }
                     ]
                   , image =
                     Config.Image.emptySpace
                   }
               }
           else if
             repairResult < 0.666
           then
             let
               ship =
                 model1.ship
             in
               Model
                 { model1
                 |  currentEvent =
                   Just
                     { description =
                       [ "You know how to fix it, but you'd need to take apart your "
                       ++ sacrificialModule.name
                       ++ " for spare parts."
                       ]
                     , options =
                       [ { predicate =
                           always Event.Success
                         , description =
                           "Take the parts"
                         , effect =
                           ( \ model2 ->
                             Model
                               { model1
                               | ship =
                                 { ship
                                 | messageLog =
                                   [ sacrificialModule.name
                                   ++ " was disassembled"
                                   ]
                                   ++ ship.messageLog
                                 , modules =
                                   List.filter (\m -> m /= sacrificialModule) ship.modules
                                 }
                                 |> Ship.adjustCapacities
                               , currentEvent =
                                 Just
                                   { description =
                                     [ "You disassemble your "
                                     ++ sacrificialModule.name
                                     ++ ", and are able to fix your "
                                     ++ brokenModule.name
                                     ++ " with parts from it."
                                     ]
                                   , options =
                                     [ { predicate =
                                         always Event.Success
                                       , description =
                                         "Continue on"
                                       , effect =
                                         identity
                                       }
                                     ]
                                   , image =
                                     Config.Image.emptySpace
                                   }
                               }
                             )
                         }
                       , { predicate =
                           always Event.Success
                         , description =
                           "Leave it"
                         , effect =
                           ( \ (Model model2) ->
                             Model
                               { model2
                               | ship =
                                 { ship
                                 | messageLog =
                                   [ brokenModule.name
                                   ++ " was jettisoned"
                                   ]
                                   ++ ship.messageLog
                                 , modules =
                                   List.filter (\m -> m /= brokenModule) ship.modules
                                 }
                                 |> Ship.adjustCapacities
                               , currentEvent =
                                 Just
                                   { description =
                                     [ "You'd rather not risk the "
                                     ++ sacrificialModule.name
                                     ++ ", so you just throw out the broken "
                                     ++ brokenModule.name
                                     ]
                                   , options =
                                     [ { predicate =
                                         always Event.Success
                                       , description =
                                         "Continue on"
                                       , effect =
                                         identity
                                       }
                                     ]
                                   , image =
                                     Config.Image.emptySpace
                                   }
                               }
                             )
                         }
                       ]
                     , image =
                       Config.Image.emptySpace
                     }
                 }
           else
             let
               ship =
                 model1.ship
             in
               Model
                 { model1
                 |  currentEvent =
                   Just
                     { description =
                       [ "You fiddle around with it, and it explodes! Your "
                       ++ sacrificialModule.name
                       ++ " is destroyed in the explosion."
                       ]
                     , options =
                       [ { predicate =
                           always Event.Success
                         , description =
                           "Continue on"
                         , effect =
                           ( \ (Model model2) ->
                             Model
                               { model1
                               | ship =
                                 { ship
                                 | messageLog =
                                   [ brokenModule.name
                                   ++ " exploded!"
                                   ,  sacrificialModule.name
                                   ++ " was destroyed"
                                   ]
                                   ++ ship.messageLog
                                 , modules =
                                   List.filter (\m -> m /= brokenModule && m /= sacrificialModule) ship.modules
                                 }
                                 |> Ship.adjustCapacities
                               }
                           )
                         }
                       ]
                     , image =
                       Config.Image.emptySpace
                     }
                 }
         )
      }
    , { predicate =
        ( \ (Model model1) ->
          if
            Ship.canRepair brokenModuleIndex model1.ship
          then
            Event.Success
          else
            Event.Hidden
        )
      , description =
        "Use your Auto-Repair system"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | currentEvent =
              Just
                { description =
                  [ "Your Auto-Repair system fixes the broken part."
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
        )
      }
    ]
  , image =
    Config.Image.emptySpace
  }

event : Event.SpecialPooled (Event.Journey.InProgress (Event.Pool.Course Event.Pool.ModelWrapper)) Model
event { model } =
  Just
    ( toFloat (List.length model.ship.modules) - 1
    , case
        List.indexedMap Tuple.pair model.ship.modules
      of
        firstModule :: modules ->
          Random.map3
            buildStructure
            (Random.uniform firstModule modules)
            (Random.float 0 1)
            (Random.uniform firstModule modules)
        [] ->
          Random.constant
            { description =
              [ "Your ship is making odd noises for a moment, but then they stop."
              ]
            , options =
              [ { predicate =
                  always Event.Success
                , description =
                  "Continue onwards"
                , effect =
                  identity
                }
              ]
            , image =
              Config.Image.emptySpace
            }
        )
