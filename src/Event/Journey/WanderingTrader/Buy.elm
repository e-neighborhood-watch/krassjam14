module Event.Journey.WanderingTrader.Buy exposing
  ( event
  )

import Random

import Cash
import Config.Image
import Event
import Extra.String as String
import Image
import Model exposing
  ( Model (..)
  )
import Ship

buildStructure wantedModule =
  { description =
    [ "A wandering trader hails you. They say they want your "
    ++ wantedModule.name
    ++ ", and offer "
    ++ ( wantedModule.price
       |> Cash.map ((*) 2)
       |> String.fromCash
       )
    ++ " in exchange."
    ]
  , options =
    [ { predicate =
        always Event.Success
      , description =
        "Take the offer"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | ship =
              let
                ship =
                  model1.ship
              in
                { ship
                | messageLog =
                  [ wantedModule.name
                  ++ " was sold"
                  ]
                  ++ ship.messageLog
                , modules =
                  List.filter (\p -> p /= wantedModule) ship.modules
                , cash =
                  wantedModule.price
                  |> Cash.map ((*) 2)
                  |> Cash.add ship.cash
                }
                |> Ship.adjustCapacities
            , currentEvent =
              Just
                { description =
                  [ "The trader gives you "
                  ++ ( wantedModule.price
                     |> Cash.map ((*) 2)
                     |> String.fromCash
                     )
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
        )
      }
     , { predicate =
        always Event.Success
      , description =
        "Decline the offer"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | currentEvent =
              Just
                { description =
                  [ "The trader leaves."
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
          )
      }
    ]
  , image =
    Config.Image.emptySpace
  }


event { model } =
  Just
    ( toFloat (List.length model.ship.modules) * 2 / 3
    , case
        model.ship.modules
      of
        firstModule :: modules ->
          Random.map
            buildStructure
            (Random.uniform firstModule modules)
        [] ->
          Random.constant
            { description =
              [ "A wandering trader passes by. They don't seem interested in you."
              ]
            , options =
              [ { predicate =
                  always Event.Success
                , description =
                  "Continue onwards"
                , effect =
                  identity
                }
              ]
            , image =
              Config.Image.emptySpace
            }
        )

