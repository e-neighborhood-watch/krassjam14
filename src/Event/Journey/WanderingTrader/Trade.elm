module Event.Journey.WanderingTrader.Trade exposing
  ( event
  )

import Random

import Config.Image
import Event
import Event.Journey
import Event.Pool
import Image
import Model exposing
  ( Model (..)
  )
import Module exposing
  ( Module
  )
import Render.Module as Module
import Ship


buildStructure : Module -> Module -> Module -> Event.Event Model
buildStructure wantedModule swapOne swapTwo =
  { description =
    [ "A wandering trader hails you. They say they want your "
    ++ wantedModule.name
    ++ ", and offer upgrades for your ship in exchange."
    ]
  , options =
    [ { predicate =
        always Event.Success
      , description =
        "Take the offer"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | ship =
              let
                ship =
                  model1.ship
              in
                { ship
                | messageLog =
                  [ wantedModule.name
                  ++ " was sold", "Installed "
                  ++ swapOne.name
                  ++ " on ship"
                  , "Installed "
                  ++ swapTwo.name
                  ++ " on ship"
                  ]
                  ++ ship.messageLog
                , modules =
                  List.filter (\p -> p /= wantedModule) ship.modules ++ [swapOne, swapTwo]
                }
                |> Ship.adjustCapacities
            , currentEvent =
              Just
                { description =
                  [ "The trader gives you "
                  ++ swapOne.name
                  ++ " and "
                  ++ swapTwo.name
                  ++ " in return."
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
        )
      }
    ,  { predicate =
         always Event.Success
       , description =
         "View part"
       , effect =
         ( \ (Model model1) ->
           Model
             { model1
             | currentEvent =
               Just
                 { description =
                   Module.render () wantedModule
                 , options =
                   [ { predicate =
                       always Event.Success
                     , description =
                       "Go back"
                     , effect =
                       ( \ (Model model2) ->
                         Model
                           { model2
                           | currentEvent =
                             buildStructure wantedModule swapOne swapTwo
                             |> Just
                           }
                       )
                     }
                   ]
                 , image =
                   wantedModule.image
                 }
             }
         )
       }
    , { predicate =
        always Event.Success
      , description =
        "Decline the offer"
      , effect =
        ( \ (Model model1) ->
          Model
            { model1
            | currentEvent =
              Just
                { description =
                  [ "The trader leaves."
                  ]
                , options =
                  [ { predicate =
                      always Event.Success
                    , description =
                      "Continue on"
                    , effect =
                      identity
                    }
                  ]
                , image =
                  Config.Image.emptySpace
                }
            }
        )
      }
    ]
  , image =
    Config.Image.emptySpace
  }

event : Event.SpecialPooled (Event.Journey.InProgress (Event.Pool.Course Event.Pool.ModelWrapper)) Model
event { model } =
    Just
      ( toFloat (List.length model.ship.modules) * 2 / 3
      , case
          model.ship.modules
        of
          firstModule :: modules ->
            Random.map3
              buildStructure
              (Random.uniform firstModule modules)
              Module.generator
              Module.generator
          [] ->
            Random.constant
              { description =
                [ "A wandering trader passes by. They don't seem interested in you."
                ]
              , options =
                [ { predicate =
                    always Event.Success
                  , description =
                    "Continue onwards"
                  , effect =
                    identity
                  }
                ]
              , image =
                Config.Image.emptySpace
              }
          )

