module View exposing
  ( view
  )

import Browser
import Css
import Html.Styled as Html exposing
  ( Html
  )
import Html.Styled.Attributes as HtmlAttr
import Svg.Styled as Svg exposing
  ( svg
  )
import Svg.Styled.Attributes as SvgAttr


import Cash
import Config
import Event
import Extra.List as List
import Extra.String as String
import Image exposing
  ( Image
  )
import Load exposing
  ( Loaded (..)
  )
import Local
import Message exposing
  ( Message (..)
  )
import Model exposing
  ( Model (..)
  , ShipPosition (..)
  , Info (..)
  , idToNode
  , selectedNode
  , infoToMaybe
  )
import Node exposing
  ( Node
  )
import Passenger exposing
  ( Passenger
  , Breathability (..)
  )
import Port
import Render.Module as Module
import Render.Passenger as Passenger
import Render.Port as Port
import Ship exposing
  ( TravelStatus (..)
  )


title : String
title =
  "The game!"


textBox : List String -> Html Message
textBox =
  List.map Html.text
  >> List.intersperse (Html.br [] [])
  >> Html.div
    [ HtmlAttr.css
      [ 50 |> Css.pct |> Css.height
      ]
    ]


view : Model -> Browser.Document Message
view (Model model) =
  let
    textBoxes : Html Message
    textBoxes =
      Html.div
        [ HtmlAttr.css
          [ Css.displayFlex
          , Css.flexShrink (Css.num 0)
          , Css.flexDirection
            Css.column
          , 25 |> Css.pct |> Css.width
          ]
        ]
        [ textBox
          [ Local.text.cash
            ++ ": "
            ++ String.fromCash model.ship.cash
          , Local.text.weight
            ++ ": "
            ++ String.fromWeight
              ( model.ship
              |> Ship.totalWeight
              )
          , Local.text.fuel
            ++ ": "
            ++
              ( model.ship.fuel
              |> String.fromFloatRound 3
              )
            ++ "/"
            ++
              ( model.ship
              |> Ship.totalFuelCapacity
              |> String.fromFloatRound 3
              )
          , Local.text.passengers
            ++ ": "
            ++ ( model.ship.passengers
              |> List.length
              |> String.fromInt
              )
            ++ "/"
            ++ ( model.ship
              |> Ship.totalPassengerSlots
              |> String.fromInt
              )
          , Local.text.speed
            ++ ": "
            ++ Config.symbols.distance
            ++ "/"
            ++ Config.symbols.time
            ++ ( model.ship
              |> Ship.speed
              |> String.fromFloatRound 3
              )
          ]
        , textBox
          model.ship.messageLog
        ]


    -- For debugging
    drawRange : Node -> Svg.Svg Message
    drawRange { x, y, r } =
      Svg.circle
        [ SvgAttr.cx
          (String.fromFloat x)
        , SvgAttr.cy
          (String.fromFloat y)
        , SvgAttr.r
          ( model.ship
          |> Ship.totalFuelCapacity
          |> String.fromFloat
          )
        , SvgAttr.fill
          Config.colors.dim
        , SvgAttr.fillOpacity
          "0.5"
        , SvgAttr.stroke
          Config.colors.bright
        , SvgAttr.strokeWidth
          "0.005"
        ]
        []

    drawShip : Svg.Svg Message
    drawShip =
      case
        model.shipPosition
      of
        AtPort id ->
          case
            model.loadingRequired
          of
            Loading ->
              Svg.g [] []
            Ready loads ->
              case
                List.head (List.drop (Node.unId id) loads.nodes)
              of
                Nothing ->
                  Svg.g [] []
                Just { x, y } ->
                  Svg.circle
                    [ x
                      |> String.fromFloat
                      |> SvgAttr.cx
                    , y
                      |> String.fromFloat
                      |> SvgAttr.cy
                    , Config.sizes.ship
                      |> String.fromFloat
                      |> SvgAttr.r
                    , SvgAttr.strokeWidth
                      "0.015"
                    , SvgAttr.stroke
                      Config.colors.bright
                    , SvgAttr.fill
                      "none"
                    ]
                    [ ]
        InTransit {start, end, progress} ->
          Svg.circle
            [ ((idToNode model start).x * (1 - progress) + (idToNode model end).x * progress)
              |> String.fromFloat
              |> SvgAttr.cx
            , ((idToNode model start).y * (1 - progress) + (idToNode model end).y * progress)
              |> String.fromFloat
              |> SvgAttr.cy
            , Config.sizes.ship
              |> String.fromFloat
              |> SvgAttr.r
            , SvgAttr.strokeWidth
              "0.015"
            , SvgAttr.stroke
              Config.colors.bright
            , SvgAttr.fill
              "none"
            ]
            [ ]

    drawCursor : Svg.Svg Message
    drawCursor =
      let
        reticuleSize =
          Config.sizes.reticule


        node =
          selectedNode model
      in
        Svg.g
          []
          ( [ Svg.rect
              [ SvgAttr.x
                (String.fromFloat (model.cursorPosition.x - reticuleSize / 2))
              , SvgAttr.y
                (String.fromFloat (model.cursorPosition.y - reticuleSize / 2))
              , SvgAttr.width
                (String.fromFloat reticuleSize)
              , SvgAttr.height
                (String.fromFloat reticuleSize)
              , SvgAttr.strokeWidth
                "0.015"
              , SvgAttr.stroke
                ( case
                    node
                  of
                   Nothing ->
                     Config.colors.dim
                   Just _ ->
                     Config.colors.bright
                )
              , SvgAttr.fill
                "none"
              ]
              [ ]
            ]
              ++ case
                  node
                 of
                   Nothing ->
                     [ ]
                   Just { name } ->
                     [ Svg.text_
                       [ model.cursorPosition.x - reticuleSize
                         |> String.fromFloat
                         |> SvgAttr.x
                       , model.cursorPosition.y
                         |> String.fromFloat
                         |> SvgAttr.y
                       , SvgAttr.strokeWidth
                         "1"
                       , SvgAttr.css
                         [ Css.fill
                           (Css.hex Config.colors.bright)
                         , Css.fontSize
                           (Css.px Config.fonts.mapFontSize)
                         , Css.property "dominant-baseline" "central"
                         , Css.property "text-anchor" "end"
                         ]
                       ]
                       [ Svg.text
                         name
                       ]
                     ]
          )


    drawNode : Node -> Svg.Svg Message
    drawNode { x, y, r , debt, id, name } =
      Svg.g
        [ ]
        (
          ( if
              case
                model.currentInfo
              of
                StationInfo cPort (Port.ClientView n) ->
                  case
                    cPort.clients
                  of
                    [ ] ->
                      False
                    aliens ->
                     case
                       List.index (modBy (List.length aliens) n) aliens
                     of
                       Nothing ->
                         False
                       Just alien ->
                         alien.destination == id
                         &&
                           (not alien.species.needTranslator
                           || Ship.hasTranslation model.ship
                           )
                PassengersInfo n _ ->
                  case
                    model.ship.passengers
                  of
                    [ ] ->
                       False
                    aliens ->
                      case
                        List.index (modBy (List.length aliens) n) aliens
                      of
                        Nothing ->
                          False
                        Just alien ->
                          alien.destination == id
                          &&
                            ( not alien.species.needTranslator
                            || Ship.hasTranslation model.ship
                            )
                _ ->
                  False
            then
              [ Svg.text_
                [ x - Config.sizes.reticule
                  |> String.fromFloat
                  |> SvgAttr.x
                , y
                  |> String.fromFloat
                  |> SvgAttr.y
                , SvgAttr.strokeWidth
                  "1"
                , SvgAttr.css
                  [ Css.fill
                    (Css.hex Config.colors.bright)
                  , Css.fontSize
                    (Css.px Config.fonts.mapFontSize)
                  , Css.property "dominant-baseline" "central"
                  , Css.property "text-anchor" "end"
                  ]
                ]
                [ Svg.text
                  name
                ]
              ]
            else
              [ ]
          )
          ++
          ( if
              Cash.unCash debt > 0
            then
              let
                fontSize =
                  Config.fonts.mapFontSize
              in
                  [ Svg.line
                    [ SvgAttr.x1
                      (x - fontSize / 2 |> String.fromFloat)
                    , SvgAttr.x2
                      (x + fontSize / 2 |> String.fromFloat)
                    , SvgAttr.y1
                      (y - fontSize / 2 |> String.fromFloat)
                    , SvgAttr.y2
                      (y + fontSize / 2 |> String.fromFloat)
                    , SvgAttr.stroke
                      Config.colors.bright
                    , SvgAttr.strokeWidth
                      "0.015"
                    ]
                    [ ]
                  , Svg.line
                    [ SvgAttr.x1
                      (x - fontSize / 2 |> String.fromFloat)
                    , SvgAttr.x2
                      (x + fontSize / 2 |> String.fromFloat)
                    , SvgAttr.y1
                      (y + fontSize / 2 |> String.fromFloat)
                    , SvgAttr.y2
                      (y - fontSize / 2 |> String.fromFloat)
                    , SvgAttr.stroke
                      Config.colors.bright
                    , SvgAttr.strokeWidth
                      "0.015"
                    ]
                    [ ]
                  , Svg.text_
                    [ SvgAttr.x
                      (x + fontSize |> String.fromFloat)
                    , SvgAttr.y
                      (y |> String.fromFloat)
                    , SvgAttr.strokeWidth
                      "1"
                    , SvgAttr.css
                      [ Css.fill
                        (Css.hex Config.colors.bright)
                      , Css.fontSize
                        (Css.px fontSize)
                      , Css.property "dominant-baseline" "central"
                      ]
                    ]
                    [ Svg.text
                      ( "-"
                      ++ String.fromCash debt
                      )
                    ]
                  ]
            else
              [ Svg.circle
                  [ SvgAttr.cx
                    (String.fromFloat x)
                  , SvgAttr.cy
                    (String.fromFloat y)
                  , SvgAttr.r
                    (String.fromFloat r)
                  , SvgAttr.fill
                    Config.colors.bright
                  ]
                  [ ]
              ]
          )
        )


    drawConnection : Node -> Node -> List (Svg.Svg Message)
    drawConnection node1 node2 =
      let
        distance =
          Node.distance3D node1 node2
      in
        if
          distance < (model.ship |> Ship.totalFuelCapacity) &&
          distance > 0 &&
          Node.unId node1.id > Node.unId node2.id
        then
          let
            active : Bool
            active =
              case
                model.shipPosition
              of
                AtPort portId ->
                  ( Ship.travelStatus model.ship node1 node2 == OK
                  || portId == node2.id
                  )
                  &&
                  ( Ship.travelStatus model.ship node2 node1 == OK
                  || portId == node1.id
                  )
                InTransit _ ->
                  Ship.travelStatus model.ship node1 node2 == OK
                  &&
                  Ship.travelStatus model.ship node2 node1 == OK


            displayDistance : Bool
            displayDistance =
              ( ( case
                  selectedNode model
                of
                  Nothing ->
                    False
                  Just { id } ->
                    id == node1.id || id == node2.id
                )
              || ( case
                  model.shipPosition
                of
                  InTransit _ ->
                    False
                  AtPort id ->
                    id == node1.id || id == node2.id
                )
              )


            color : String
            color =
              if
                active
              then
                Config.colors.bright
              else
                Config.colors.dim


            width : String
            width =
              if
                active
              then
                "0.007"
              else
                "0.005"
          in
            ( [ Svg.line
                [ SvgAttr.x1
                  (String.fromFloat node1.x)
                , SvgAttr.y1
                  (String.fromFloat node1.y)
                , SvgAttr.x2
                  (String.fromFloat node2.x)
                , SvgAttr.y2
                  (String.fromFloat node2.y)
                , SvgAttr.stroke
                  color
                , SvgAttr.strokeWidth
                  width
                ]
                [ ]
              ] ++
                if
                  displayDistance
                then
                  [ Svg.text_
                    [ SvgAttr.css
                      [ Css.fill
                        (Css.hex color)
                      , Css.fontSize
                        (Css.px Config.fonts.mapFontSize)
                      , Css.property "dominant-baseline" "central"
                      ]
                    , (node1.x + node2.x) / 2
                      |> String.fromFloat
                      |> SvgAttr.x
                    , (node1.y + node2.y) / 2
                      |> String.fromFloat
                      |> SvgAttr.y
                    ]
                    [ distance
                      |> String.fromFloatRound 3
                      |> (++) Config.symbols.distance
                      |> Svg.text
                    ]
                  ]
                else
                  [ ]
            )
        else
          [ ]


    starMap : Html Message
    starMap =
      let
        nodes : List Node
        nodes =
          case model.loadingRequired of
            Loading ->
              []
            Ready loads ->
              loads.nodes
      in
        ( ( if
              Config.debug.displayRanges
            then
              List.map drawRange nodes
            else
              [ ]
          )
        ++ List.concat (List.liftA2 drawConnection nodes nodes)
        ++ List.map drawNode nodes
        ++ [ drawShip ]
        ++ [ drawCursor ]
        ) |>
        svg
          [ SvgAttr.css
            [ 1 |> Css.num |> Css.flexGrow
            ]
          , SvgAttr.viewBox "-1 -1 2 2"
          ]

    infoBoxes : Html Message
    infoBoxes =
      let
        imageDimension : Css.CalculatedLength
        imageDimension =
          Config.sizes.borderWidthPx
          |> (+) 5
          |> (*) 2
          |> Css.px
          |> Css.calc
            (Css.pct 100)
            Css.minus

        passengersInfo : Int -> List String
        passengersInfo index =
          case model.ship.passengers of
            [ ] ->
              [ "You have no passengers"
              , ""
              , "m"
              ++ Config.symbols.keyOptionSeparator
              ++ "Back"
              ]
            ( head :: tail ) ->
              let
                length : Int
                length = List.length model.ship.passengers
              in
                ( "Ship passengers ["
                ++ String.fromInt (modBy length index + 1)
                ++ "/"
                ++ String.fromInt length
                ++ "]"
                )
                :: ""
                ::
                  ( List.wrappedIndex index head tail
                    |> Passenger.render model
                  )
                ++
                  [ ""
                  , ","
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Previous; ."
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Next; m"
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Back"
                  ]

        modulesInfo : Int -> List String
        modulesInfo index =
          case model.ship.modules of
            [ ] ->
              [ "You have no ship"
              , ""
              , "m"
              ++ Config.symbols.keyOptionSeparator
              ++ "Back"
              ]
            ( head :: tail ) ->
              let
                length : Int
                length = List.length model.ship.modules
              in
                ( "Ship modules ["
                ++ String.fromInt (modBy length index + 1)
                ++ "/"
                ++ String.fromInt length
                ++ "]"
                )
                :: ""
                ::
                  ( List.wrappedIndex index head tail
                    |> Module.render model
                  )
                ++
                  [ ""
                  , ","
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Previous; ."
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Next; m"
                  ++ Config.symbols.keyOptionSeparator
                  ++ "Back"
                  ]

        passengerImage : Int -> Image.Image
        passengerImage index =
          case model.ship.passengers of
            [ ] ->
              Image.Empty
            ( head :: tail ) ->
              List.wrappedIndex index head tail
              |> (.species >> .image)

        moduleImage : Int -> Image
        moduleImage index =
          case model.ship.modules of
            [ ] ->
              Image.Empty
            ( head :: tail ) ->
              List.wrappedIndex index head tail
              |> (.image)
      in
        Html.div
          [ HtmlAttr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.height (Css.pct 25)
            , Css.flexShrink (Css.num 0)
            ]
          ]
          [ model.currentInfo
          |>
            infoToMaybe
              (Port.render model)
              (Event.render (Model model))
              passengersInfo
              modulesInfo
          |> Maybe.withDefault []
          |> List.map Html.text
          |> List.intersperse (Html.br [] [])
          |> Html.div
            [ HtmlAttr.css
              [ Css.flexGrow (Css.num 1)
              ]
            ]
          , Html.img
            [ model.currentInfo
            |> infoToMaybe Port.currentImage  (.image) passengerImage moduleImage
            |> Maybe.withDefault Image.Empty
            |> Image.toSrc
            |> HtmlAttr.src
            , HtmlAttr.css
              [ Css.height imageDimension
              , Css.borderStyle Css.solid
              , Css.borderWidth (Css.px Config.sizes.borderWidthPx)
              , Css.margin (Css.px 5)
              ]
            ]
            []
          ]

    mapAndAlien : Html Message
    mapAndAlien =
      Html.div
        [ HtmlAttr.css
          [ Css.displayFlex
          , Css.flexDirection Css.column
          , Css.flexGrow (Css.num 1)
          ]
        ]
        [ starMap
        , infoBoxes
        ]

    body : List (Html Message)
    body =
      [ Html.div
        [ HtmlAttr.css
          [ Css.displayFlex
          , Css.flexDirection
            Css.row
          , Css.position
            Css.fixed
          , 100 |> Css.pct |> Css.height
          , 100 |> Css.pct |> Css.width
          , Css.top
            Css.zero
          , Css.left
            Css.zero
          , Css.fontFamily
            Css.monospace
          , Css.fontWeight
            Css.bold
          , Css.fontSize
            Css.large
          , Css.color
            (Css.hex Config.colors.bright)
          , Css.backgroundColor
            (Css.hex Config.colors.empty)
          , Css.borderColor
            (Css.hex Config.colors.bright)
          ]
        ]
        [ textBoxes
        , mapAndAlien
        ]
      ]
  in
    { title = title
    , body = List.map Html.toUnstyled body
    }

