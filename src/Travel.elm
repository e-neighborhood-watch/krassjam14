module Travel exposing
  ( travel
  )

import Random

import Cash exposing
  ( Cash (..)
  )
import Config
import Event
import Event.Pool.Arrival
import Event.Pool.Journey
import Extra.List as List
import Extra.String as String
import Image
import Load exposing
  ( Loaded (..)
  )
import Message exposing
  ( Message (..)
  )
import Model exposing
  ( ModelRecord
  , Model (..)
  , ShipPosition (..)
  )
import Module
import Node
import Passenger
import Port
import Ship exposing
  ( Ship
  )

eventPool : List (Event.Pooled Model)
eventPool =
  List.map
    ( \ arrivalPooled (Model model) ->
      case
        model.loadingRequired
      of
        Loading ->
          Nothing
        Ready loaded ->
          case
            model.shipPosition
          of
            AtPort _ ->
              Nothing
            InTransit { start, end, progress } ->
              if
                progress < 1
              then
                Nothing
              else
                arrivalPooled
                  { model =
                    model
                  , start =
                    start
                  , end =
                    end
                  , loaded =
                    loaded
                  }
    )
    Event.Pool.Arrival.pool
  ++
    List.map
    ( \ journeyPooled (Model model) ->
      case
        model.loadingRequired
      of
        Loading ->
          Nothing
        Ready loaded ->
          case
            model.shipPosition
          of
            AtPort _ ->
              Nothing
            InTransit { start, end, progress } ->
              if
                progress >= 1
              then
                Nothing
              else
                journeyPooled
                  { model =
                    model
                  , start =
                    start
                  , end =
                    end
                  , loaded =
                    loaded
                  , progress =
                    progress
                  }
    )
    Event.Pool.Journey.pool


travel : ModelRecord -> (ModelRecord, Cmd Message)
travel model =
  case
    model.currentEvent
  of
    Just _ ->
      (model, Cmd.none)
    Nothing ->
      case
        model.shipPosition
      of
        AtPort _ ->
          (model, Cmd.none)
        InTransit { start, end, progress } ->
          case
            model.loadingRequired
          of
            Loading ->
              (model, Cmd.none)
            Ready { nodes } ->
              case
                start
              of
                Node.Id startId ->
                  case
                    List.index startId nodes
                  of
                    Nothing ->
                      (model, Cmd.none)
                    Just origin ->
                      case
                        end
                      of
                        Node.Id endId ->
                          case
                            List.index endId nodes
                          of
                            Nothing ->
                              (model, Cmd.none)
                            Just destination ->
                              let
                                totalDistance : Float
                                totalDistance =
                                  Node.distance3D origin destination

                                ship : Ship
                                ship =
                                  model.ship

                                shipSpeed : Float
                                shipSpeed =
                                  Ship.speed ship

                                distanceAbleToTravel : Float
                                distanceAbleToTravel =
                                  min ship.fuel shipSpeed

                                progressMade : Float
                                progressMade =
                                  distanceAbleToTravel / totalDistance

                                newProgress : Float
                                newProgress =
                                  progress + progressMade

                                distanceTraveled : Float
                                distanceTraveled =
                                  if
                                    newProgress > 1
                                  then
                                    (1 - progress) * totalDistance
                                  else
                                    if
                                      newProgress < 0
                                    then
                                      (0 - progress) * totalDistance
                                    else
                                      distanceAbleToTravel

                                newModel : ModelRecord
                                newModel =
                                  { model
                                  | shipPosition =
                                    InTransit
                                      { start =
                                        start
                                      , end =
                                        end
                                      , progress =
                                        newProgress
                                        |> max 0
                                        |> min 1
                                      }
                                  , ship =
                                    { ship
                                    | fuel =
                                      ship.fuel - distanceTraveled
                                    }
                                  }
                              in
                                ( newModel
                                , eventPool
                                  |> Event.generatorFromPool (Model newModel)
                                  |> Random.generate
                                  RandomEvent
                                )
