module Module exposing
  ( Module
  , Perk (..)
  , generator
  , startingPortGenerator
  )

import Random

import Dict

import Cash exposing
  ( Cash (..)
  )
import Config
import Extra.Random as Random
import Image exposing
  ( Image (..)
  )
import Passenger exposing
  ( Atmosphere (..)
  )
import Weight exposing
  ( Weight (..)
  )

type Perk
  = Translator
  | Atmos Atmosphere
  | Repair
  | Shield
  | AtmosPartition


type alias Module =
  { price :
    Cash
  , weight :
    Weight
  , fuelCapacity :
    Float
  , passengerSlots :
    Int
  , thrust :
    Float
  , name :
    String
  , extraStats :
    Dict.Dict String Int
  , perks :
    List Perk
  , image:
    Image
  }


generator : Random.Generator Module
generator =
  ( Random.uniform
    randomEngine
    [ randomCabin
    , randomFuelTank
    , randomExtraModule
    ]
  )
  |> Random.join


essentialParts : Random.Generator (List Module)
essentialParts =
  Random.traverse
    identity
    [ randomEssentialFuelTank
    , randomCabin
    , randomEngine
    ]
  |> Random.andThen
    ( \ modules ->
      if
        modules
        |> List.foldr ((.price) >> Cash.add) (Cash 0)
        |> Cash.unCash
        |> (>=) 300
      then
        Random.constant modules
      else
        Random.lazy (\ _ -> essentialParts)
    )


startingPortGenerator : Random.Generator (List Module)
startingPortGenerator =
  Random.traverse
    identity
    [ randomExtraModule
    , randomExtraModule
    , generator
    ]
  |> Random.map2
    (++)
    essentialParts


randomEssentialFuelTank : Random.Generator Module
randomEssentialFuelTank =
  Random.map4
    ( \ fuzz capacity baseWeight imageIndex ->
      { price =
        ( ceiling (fuzz * capacity * 75)
        - (baseWeight * 2)
        )
        |> max 5
        |> Cash
      , passengerSlots =
        0
      , fuelCapacity =
        capacity
      , thrust =
        0.0
      , weight =
        baseWeight + (capacity * 2.0 |> ceiling)
        |> Weight
      , name =
        "Tank"
      , extraStats =
        Dict.empty
      , perks =
        [ ]
      , image =
        Image.build Image.Tank imageIndex
      }
    )
    ( Random.float 0.9 1.1
    )
    ( Random.map2
      (+)
        (Random.float 0.25 0.4)
        (Random.float 0.25 0.4)
    )
    ( Random.int 1 5
    )
    ( Random.int 0 3
    )


randomFuelTank : Random.Generator Module
randomFuelTank =
  Random.map4
    ( \ fuzz capacity baseWeight imageIndex ->
      { price =
        ( ceiling (fuzz * capacity * 80)
        - (baseWeight * 2)
        )
        |> max 5
        |> Cash
      , passengerSlots =
        0
      , fuelCapacity =
        capacity
      , thrust =
        0.0
      , weight =
        baseWeight + (capacity * 2.0 |> ceiling)
        |> Weight
      , name =
        "Tank"
      , extraStats =
        Dict.empty
      , perks =
        [ ]
      , image =
        Image.build Image.Tank imageIndex
      }
    )
    ( Random.float 0.9 1.1
    )
    ( Random.map2
      (+)
        (Random.float 0.1 0.5)
        (Random.float 0.1 0.5)
    )
    ( Random.int 1 6
    )
    ( Random.int 0 3
    )


randomCabin : Random.Generator Module
randomCabin =
  Random.map4
    ( \ fuzz capacity baseWeight imageIndex ->
      { price =
        (ceiling (fuzz * toFloat capacity * 50.0)
        - (baseWeight * 5)
        )
        |> max 5
        |> Cash
      , passengerSlots =
        capacity
      , fuelCapacity =
        0.0
      , thrust =
        0.0
      , weight =
        baseWeight + (3 * capacity)
        |> Weight
      , name =
        "Cabins"
      , extraStats =
        Dict.empty
      , perks =
        [ ]
      , image =
        Image.build Image.Cabin imageIndex
      }
    )
    ( Random.float 0.9 1.1
    )
    ( Random.int 1 12
    )
    ( Random.int 1 6
    )
    ( Random.int 0 0
    )



randomEngine : Random.Generator Module
randomEngine =
  Random.map5
    ( \ fuzz thrust baseWeight name imageIndex ->
      { price =
        (ceiling (fuzz * thrust * 25.0)
        - (baseWeight * 5)
        )
        |> max 5
        |> Cash
      , thrust =
        thrust
      , fuelCapacity =
        0.0
      , passengerSlots =
        0
      , weight =
        baseWeight + ceiling (thrust * 0.3)
        |> Weight
      , name =
        name
      , extraStats =
        Dict.empty
      , perks =
        [ ]
      , image =
        Image.build Image.Engine imageIndex
      }
    )
    ( Random.float 0.9 1.1
    )
    ( Random.float 1.0 41.8
    )
    ( Random.int 1 12
    )
    ( Random.map2
      (++)
      ( Random.map2
        (++)
        ( Random.uniform
          "Photon"
          [ "Ion"
          , "Warp"
          , "Positron"
          , "Negative"
          , "Dark"
          ]
        )
        ( Random.uniform
          ""
          [ "-propulsion"
          ]
        )
      )
      ( Random.uniform
        " Engine"
        [ " Thruster"
        ]
      )
    )
    ( Random.int 0 1
    )


generateModule : ModuleType -> Random.Generator Module
generateModule moduleType =
  Random.map4
    ( \ price weight stats image ->
      { price =
        Cash price
      , weight =
        Weight weight
      , fuelCapacity =
        0
      , passengerSlots =
        0
      , thrust =
        0
      , name =
        moduleType.name
      , extraStats =
        Dict.fromList stats
      , perks =
        moduleType.perks
      , image =
        image
      }
    )
    moduleType.priceGenerator
    moduleType.weightGenerator
    ( Random.traverse
      ( \ (statName, statGenerator) ->
        Random.map
          ( \statValue ->
            (statName, statValue)
          )
          statGenerator
      )
      moduleType.stats
    )
    moduleType.imageGenerator


-- TODO perks and extra stats
randomExtraModule : Random.Generator Module
randomExtraModule =
  Random.andThen generateModule (Random.uniform firstModuleType allModuleTypes)

type alias ModuleType =
  { name :
    String
  , stats :
    List (String, Random.Generator Int)
  , priceGenerator :
    Random.Generator Int
  , weightGenerator :
    Random.Generator Int
  , perks :
    List Perk
  , imageGenerator :
    Random.Generator Image
  }


firstModuleType : ModuleType
firstModuleType =
  { name =
    "Universal Translator"
  , stats =
    [ ]
  , priceGenerator =
    Random.int 40 55
  , weightGenerator =
      Random.weighted
         (1, 1)
         [ (2, 2)
         , (3, 3)
         ]
  , perks =
    [ Translator
    ]
  , imageGenerator =
    Random.uniform
      ( Image.build Image.Module 0 )
      [ Image.build Image.Module 1
      , Image.build Image.Module 2
      , Image.build Image.Module 3
      , Image.build Image.Module 4
      , Image.build Image.Module 5
      , Image.build Image.Tank 0
      , Image.build Image.Tank 1
      ]
  }


allModuleTypes : List ModuleType
allModuleTypes =
  [ { name =
      "Oxygen Supplier"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 10 30
    , weightGenerator =
      Random.int 5 12
    , perks =
      [ Atmos Oxygen
      ]
    , imageGenerator =
      Image.build Image.Atmosphere 0
      |> Random.constant
    }
  , { name =
      "Xenon Supplier"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 20 40
    , weightGenerator =
      Random.int 5 12
    , perks =
      [ Atmos Xenon
      ]
    , imageGenerator =
      Image.build Image.Atmosphere 1
      |> Random.constant
    }
  , { name =
      "Atmosphere Generator C"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 30 60
    , weightGenerator =
      Random.int 5 12
    , perks =
      [ Atmos UF6
      ]
    , imageGenerator =
      Image.build Image.Atmosphere 2
      |> Random.constant
    }
  , { name =
      "Atmospheric Partitioner"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 20 40
    , weightGenerator =
      Random.int 1 6
    , perks =
      [ AtmosPartition
      ]
    , imageGenerator =
      Image.build Image.Tank 3
      |> Random.constant
    }
  -- , { name =
  --     "Gourmet Food Synthesizer"
  --   , stats =
  --     [ ("taste", Random.int 0 255)
  --     ]
  --   , priceGenerator =
  --     Random.int 15 80
  --   , weightGenerator =
  --     Random.int 2 8
  --   , perks =
  --     [ ]
  --   , imageGenerator =
  --     Random.uniform
  --       "module4.png"
  --       [ "module3.png"
  --       , "module0.png"
  --       ]
  --   }
  -- , { name =
  --     "Music System"
  --   , stats =
  --     [ ("volume", Random.int 0 100)
  --     , ("genre", Random.int -7000 7000)
  --     ]
  --   , priceGenerator =
  --     Random.int 40 60
  --   , weightGenerator =
  --     Random.int 1 5
  --   , perks =
  --     [ ]
  --   , imageGenerator =
  --     Random.uniform
  --       "music.png"
  --       [ "module0.png"
  --       , "module1.png"
  --       , "module2.png"
  --       , "module5"
  --       , "tank3.png"
  --       ]
  --   }
  , { name =
      "Auto Repair Unit"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 75 120
    , weightGenerator =
      Random.int 10 20
    , perks =
      [ Repair
      ]
    , imageGenerator =
      Random.uniform
        --"repair.png"
        ( Image.build Image.Module 1 )
        [ Image.build Image.Module 2
        , Image.build Image.Module 3
        , Image.build Image.Module 5
        ]
    }
  , { name =
      "Shield Generator"
    , stats =
      [ ]
    , priceGenerator =
      Random.int 50 100
    , weightGenerator =
      Random.int 8 18
    , perks =
      [ Shield
      ]
    , imageGenerator =
      Random.uniform
        ( Image.build Image.Sheild 0 )
        [ Image.build Image.Module 1
        , Image.build Image.Module 2
        , Image.build Image.Module 3
        , Image.build Image.Module 5
        ]
    }
  ]
