module Config exposing
  ( colors
  , symbols
  , debug
  , fonts
  , sizes
  , speeds
  , misc
  , gameplay
  )

type alias ColorPalette =
  { bright :
    String
  , dim :
    String
  , empty :
    String
  }


type alias DebugOptions =
  { displayRanges :
    Bool
  , elmReactor :
    Bool
  }


type alias FontOptions =
  { mapFontSize :
    Float
  }


type alias Sizes =
  { borderWidthPx :
    Float
  , reticule :
    Float
  , ship :
    Float
  , nodeHitBox :
    Float
  }


type alias Speeds =
  { cursor :
    Float
  }


gameplay :
  { debtRate :
    Float
  }
gameplay =
  { debtRate =
    1.3
  }


colors : ColorPalette
colors =
  { bright =
    "#09aa14"
  , dim =
    "#004000"
  , empty =
    "#000000"
  }


symbols :
  { currency :
    String
  , weight :
    String
  , distance :
    String
  , time :
    String
  , keyOptionSeparator :
    String
  }
symbols =
  { currency =
    "θ"
  , weight =
    "Γ"
  , distance =
    "Δ"
  , time =
    "D"
  , keyOptionSeparator =
    " - "
  }


fonts : FontOptions
fonts =
  { mapFontSize =
    0.05
  }


sizes : Sizes
sizes =
  { borderWidthPx =
    3
  , reticule =
    0.075
  , ship =
    0.05
  , nodeHitBox =
    0.075
  }


speeds : Speeds
speeds =
  { cursor = 0.019
  }


debug : DebugOptions
debug =
  { displayRanges =
    False
  , elmReactor =
    False
  }


misc :
  { compactMenus :
    Bool
  }
misc =
  { compactMenus =
    True
  }
